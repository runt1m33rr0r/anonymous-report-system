package com.primary.server.controllers;

import com.primary.server.models.login.AdminLoginModel;
import com.primary.server.models.login.ClientLoginModel;
import com.primary.server.models.login.ClientLoginResultModel;
import com.primary.server.models.registration.UserRegistrationModel;
import com.primary.server.models.user.ChangePasswordModel;
import com.primary.server.services.UserService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class AuthController {

    private final UserService userService;

    @PostMapping("/login")
    public ClientLoginResultModel login(@RequestBody @Valid ClientLoginModel clientBundle) {
        return this.userService.login(clientBundle);
    }

    @PostMapping("/admin")
    public ClientLoginResultModel loginAdmin(@RequestBody @Valid AdminLoginModel adminLogin) {
        return this.userService.login(adminLogin);
    }

    @PostMapping("/register")
    public void register(@RequestBody @Valid UserRegistrationModel registrationModel) {
        this.userService.createUser(registrationModel);
    }

    @PutMapping("/password")
    public void changePassword(@RequestBody @Valid ChangePasswordModel changePasswordModel,
                                @RequestHeader("user") String username) {
        this.userService.changePassword(username, changePasswordModel);
    }

}
