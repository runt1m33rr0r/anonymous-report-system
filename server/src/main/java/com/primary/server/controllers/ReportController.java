package com.primary.server.controllers;

import com.primary.server.entities.ReportStatus;
import com.primary.server.entities.Role;
import com.primary.server.models.reports.CreateReportModel;
import com.primary.server.models.reports.ReportListModel;
import com.primary.server.models.reports.ReportModel;
import com.primary.server.services.ReportService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reports")
@AllArgsConstructor
public class ReportController {

    private ReportService reportService;

    @PostMapping
    public void createReport(@RequestBody @Valid CreateReportModel reportModel,
                             @RequestHeader("user") String username) {
        this.reportService.createReport(username, reportModel);
    }

    @GetMapping
    public ReportListModel getReports(@RequestParam("status") ReportStatus status,
                                      @RequestParam(value = "page", defaultValue = "0") int page,
                                      @RequestHeader("user") String username,
                                      @RequestHeader("role") String role) {
        return this.reportService.getReports(status, username, Role.valueOf(role), page);
    }

    @GetMapping("/{reportId}")
    public ReportModel getReport(@PathVariable("reportId") Long reportId,
                                 @RequestHeader("user") String username,
                                 @RequestHeader("role") String role) {
        return this.reportService.getReport(reportId, username, Role.valueOf(role));
    }

}
