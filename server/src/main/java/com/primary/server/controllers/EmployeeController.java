package com.primary.server.controllers;

import com.primary.server.models.employees.EmployeeContactModel;
import com.primary.server.models.registration.UserRegistrationModel;
import com.primary.server.models.user.UserKeyModel;
import com.primary.server.services.EmployeeService;
import com.primary.server.services.UserService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employees")
@AllArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;
    private final UserService userService;

    @PostMapping
    public void createEmployee(@RequestBody @Valid UserRegistrationModel registrationModel) {
        this.employeeService.createEmployee(registrationModel);
    }

    @GetMapping
    public EmployeeContactModel getEmployee() {
        return this.employeeService.getEmployee();
    }

    @GetMapping("/{username}")
    public UserKeyModel getEmployeeKey(@PathVariable String username) {
        return this.userService.getUserKey(username);
    }

}
