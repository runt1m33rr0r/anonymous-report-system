package com.primary.server.controllers;

import com.primary.server.entities.Role;
import com.primary.server.models.comments.CommentModel;
import com.primary.server.models.comments.CreateCommentModel;
import com.primary.server.services.CommentService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comments")
@AllArgsConstructor
public class CommentController {

    private CommentService commentService;

    @GetMapping
    public List<CommentModel> getComments(@RequestParam("reportId") Long id,
                                          @RequestHeader("user") String username,
                                          @RequestHeader("role") String role) {
        return this.commentService.getComments(id, username, Role.valueOf(role));
    }

    @PostMapping
    public CommentModel createComment(@RequestBody @Valid CreateCommentModel commentModel,
                              @RequestHeader("user") String username,
                              @RequestHeader("role") String role) {
        return this.commentService.createComment(commentModel, username, Role.valueOf(role));
    }

}
