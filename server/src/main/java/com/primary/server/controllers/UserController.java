package com.primary.server.controllers;

import com.primary.server.models.user.UserKeyModel;
import com.primary.server.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @GetMapping("/{username}")
    public UserKeyModel getUserKey(@PathVariable String username) {
        return this.userService.getUserKey(username);
    }

}
