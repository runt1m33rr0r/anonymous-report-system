package com.primary.server.models.comments;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentModel {

    private long id;

    @NotEmpty
    private String author;

    @NotEmpty
    private String content;

    @NotNull
    private LocalDateTime creationTime;

    private String attachedFileName;

}