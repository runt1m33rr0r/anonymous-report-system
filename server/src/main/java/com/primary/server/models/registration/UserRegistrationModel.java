package com.primary.server.models.registration;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistrationModel {

    @NotNull(message = "Username is missing")
    @Length(min = 1, max = 30, message = "Username can be between 1 and 30 symbols in length")
    private String username;

    @NotNull(message = "Password is missing")
    @Length(min = 6, max = 30, message = "Password can be between 6 and 30 symbols in length")
    private String password;

}
