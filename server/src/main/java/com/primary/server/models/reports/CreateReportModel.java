package com.primary.server.models.reports;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateReportModel {

    @NotNull
    private String title;

    @NotNull
    private String titleCopy;

    @NotNull
    private String content;

    @NotNull
    private String contentCopy;

    @NotNull
    private String agentName;

}
