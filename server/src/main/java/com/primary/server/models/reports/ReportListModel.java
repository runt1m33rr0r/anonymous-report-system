package com.primary.server.models.reports;

import com.primary.server.entities.ReportStatus;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReportListModel {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReportModel {

        private Long id;

        @NotNull
        private String author;

        @NotNull
        private String agent;

        @NotNull
        private String title;

        @NotNull
        private LocalDate creationDate;

        private ReportStatus status;

    }

    @NotNull
    private List<ReportModel> reports;

    private int pagesCount;

}
