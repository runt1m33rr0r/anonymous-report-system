package com.primary.server.models.login;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdminLoginModel {

    @NotNull
    @Size(min=1, max=15)
    private String username;

    @NotNull
    @Size(min=4, max=30)
    private String password;

}
