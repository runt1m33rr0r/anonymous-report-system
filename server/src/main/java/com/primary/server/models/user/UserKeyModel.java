package com.primary.server.models.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserKeyModel {
    private byte[] basePublicKey;
    private byte[] identityPublicKey;
}
