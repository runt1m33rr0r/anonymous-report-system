package com.primary.server.repositories;

import com.primary.server.entities.User;
import com.primary.server.entities.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User getFirstByUsername(String username);

    boolean existsByUsername(String username);

    int countUsersByRole(Role role);

}