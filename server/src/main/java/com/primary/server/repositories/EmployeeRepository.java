package com.primary.server.repositories;

import com.primary.server.entities.Employee;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    @Query("select e from Employee e where e.user.clientKeyBundle is not null order by size(e.assignedReports)")
    List<Employee> findByLeastAssignedReports(Pageable pageable);

    Employee getByUserUsername(String username);

}
