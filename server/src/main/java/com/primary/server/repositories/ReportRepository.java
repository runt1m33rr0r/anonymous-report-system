package com.primary.server.repositories;

import com.primary.server.entities.Report;
import com.primary.server.entities.ReportStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends CrudRepository<Report, Long> {

    Page<Report> findAllByAuthorUsernameAndReportStatus(String authorUsername,
                                                        ReportStatus reportStatus,
                                                        Pageable pageable);

    Page<Report> findAllByAgentUserUsernameAndReportStatus(String agentUsername,
                                                           ReportStatus reportStatus,
                                                           Pageable pageable);

}
