package com.primary.server.repositories;

import com.primary.server.entities.ClientKeyBundle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

@Repository
public interface SignalKeyRepository extends CrudRepository<ClientKeyBundle, Long> {

    void delete(@NonNull ClientKeyBundle keyBundle);

}
