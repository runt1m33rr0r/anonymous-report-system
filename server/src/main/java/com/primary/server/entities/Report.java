package com.primary.server.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Report extends BaseEntity {

    @NotNull
    @ManyToOne
    private User author;

    @NotNull
    @ManyToOne
    private Employee agent;

    @NotNull
    private String title;

    @NotNull
    private String titleCopy;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ReportStatus reportStatus;

    @Temporal(TemporalType.DATE)
    private LocalDate creationDate;

}
