package com.primary.server.entities;

public enum ReportStatus {
    OPEN,
    IN_PROGRESS,
    CLOSED
}
