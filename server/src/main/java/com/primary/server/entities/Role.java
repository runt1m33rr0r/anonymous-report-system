package com.primary.server.entities;

public enum Role {
    ADMIN, EMPLOYEE, USER
}