package com.primary.server.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Comment extends BaseEntity {

    @NotNull
    @ManyToOne
    private User author;

    @NotNull
    @ManyToOne
    private Report report;

    @NotEmpty
    private String content;

    @NotEmpty
    private String contentCopy;

    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime creationTime;

    private String attachedFileName;

}
