package com.primary.server.entities;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientKeyBundle extends BaseEntity {

    @NotNull
    private String basePublicKey;

    @NotNull
    private String identityPublicKey;

}
