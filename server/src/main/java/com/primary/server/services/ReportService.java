package com.primary.server.services;

import com.primary.server.entities.ReportStatus;
import com.primary.server.entities.Role;
import com.primary.server.models.reports.CreateReportModel;
import com.primary.server.models.reports.ReportListModel;
import com.primary.server.models.reports.ReportModel;

public interface ReportService {

    void createReport(String authorName, CreateReportModel reportModel);

    ReportListModel getReports(ReportStatus status, String username, Role userRole, int pageNumber);

    ReportModel getReport(Long reportId, String username, Role userRole);

}
