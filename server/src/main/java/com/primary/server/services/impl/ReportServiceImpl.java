package com.primary.server.services.impl;

import com.primary.server.entities.*;
import com.primary.server.exceptions.ValidationException;
import com.primary.server.models.reports.CreateReportModel;
import com.primary.server.models.reports.ReportListModel;
import com.primary.server.models.reports.ReportModel;
import com.primary.server.repositories.CommentRepository;
import com.primary.server.repositories.EmployeeRepository;
import com.primary.server.repositories.ReportRepository;
import com.primary.server.repositories.UserRepository;
import com.primary.server.services.ReportService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ReportServiceImpl implements ReportService {

    private UserRepository userRepository;
    private EmployeeRepository employeeRepository;
    private ReportRepository reportRepository;
    private CommentRepository commentRepository;

    @Override
    @Transactional
    public void createReport(String authorName, CreateReportModel reportModel) {
        User author = this.userRepository.getFirstByUsername(authorName);
        if (author == null) {
            throw new ValidationException("Author does not exist");
        }

        Employee agent = this.employeeRepository.getByUserUsername(reportModel.getAgentName());
        if (agent == null) {
            throw new ValidationException("Could not find selected agent");
        }

        Report savedReport = this.reportRepository.save(
                new Report(
                        author,
                        agent,
                        reportModel.getTitle(),
                        reportModel.getTitleCopy(),
                        ReportStatus.OPEN,
                        LocalDate.now())
        );
        Comment authorComment = new Comment(
                author,
                savedReport,
                reportModel.getContent(),
                reportModel.getContentCopy(),
                LocalDateTime.now(),
                null);
        this.commentRepository.save(authorComment);

        agent.getAssignedReports().add(savedReport);
        this.employeeRepository.save(agent);
    }

    @Override
    public ReportListModel getReports(ReportStatus status, String username, Role userRole, int pageNumber) {
        if (pageNumber < 0) {
            throw new ValidationException("Page number can't be less than 0");
        }

        PageRequest pageData = PageRequest.of(
                pageNumber,
                1,
                Sort.by(new Sort.Order(Sort.Direction.DESC, "creationDate")));
        Page<Report> reports;
        List<ReportListModel.ReportModel> reportModelList;
        if (userRole == Role.USER) {
            reports = this.reportRepository.findAllByAuthorUsernameAndReportStatus(
                    username,
                    status,
                    pageData);
            reportModelList = reports
                    .map(report -> new ReportListModel.ReportModel(
                            report.getId(),
                            report.getAuthor().getUsername(),
                            report.getAgent().getUser().getUsername(),
                            report.getTitleCopy(),
                            report.getCreationDate(),
                            status))
                    .toList();
        } else if (userRole == Role.EMPLOYEE) {
            reports = this.reportRepository.findAllByAgentUserUsernameAndReportStatus(
                    username,
                    status,
                    pageData);
            reportModelList = reports
                    .map(report -> new ReportListModel.ReportModel(
                            report.getId(),
                            report.getAuthor().getUsername(),
                            report.getAgent().getUser().getUsername(),
                            report.getTitle(),
                            report.getCreationDate(),
                            status))
                    .toList();
        } else {
            throw new ValidationException("Invalid user type trying to access reports");
        }

        return new ReportListModel(reportModelList, reports.getTotalPages());
    }

    @Override
    public ReportModel getReport(Long reportId, String username, Role userRole) {
        if (userRole == Role.ADMIN) {
            throw new ValidationException("Admins cant read reports");
        }

        User user = this.userRepository.getFirstByUsername(username);
        if (user == null) {
            throw new ValidationException("User not found");
        }

        Optional<Report> report = this.reportRepository.findById(reportId);
        if (report.isEmpty()) {
            throw new ValidationException("Report does not exist");
        }

        if (!report.get().getAuthor().getUsername().equals(username) &&
            !report.get().getAgent().getUser().getUsername().equals(username)) {
            String userType = userRole == Role.USER ? "User" : "Agent";

            throw new ValidationException(MessageFormat.format(
                    "{0} is not part of this report",
                    userType));
        }

        return new ReportModel(
                report.get().getId(),
                report.get().getAuthor().getId(),
                report.get().getAgent().getUser().getId());
    }

}
