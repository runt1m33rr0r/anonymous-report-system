package com.primary.server.services;

import com.primary.server.entities.Role;
import com.primary.server.models.comments.CommentModel;
import com.primary.server.models.comments.CreateCommentModel;

import java.util.List;

public interface CommentService {

    List<CommentModel> getComments(Long reportId, String username, Role userRole);

    CommentModel createComment(CreateCommentModel commentModel, String author, Role authorRole);

}
