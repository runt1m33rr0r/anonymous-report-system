package com.primary.server.services;

import com.primary.server.entities.Role;
import com.primary.server.entities.User;
import com.primary.server.models.login.AdminLoginModel;
import com.primary.server.models.login.ClientLoginModel;
import com.primary.server.models.login.ClientLoginResultModel;
import com.primary.server.models.registration.UserRegistrationModel;
import com.primary.server.models.user.ChangePasswordModel;
import com.primary.server.models.user.UserKeyModel;

public interface UserService {

    ClientLoginResultModel login(ClientLoginModel bundle);

    ClientLoginResultModel login(AdminLoginModel bundle);

    void createUser(UserRegistrationModel registrationModel);

    User registerUser(UserRegistrationModel registrationModel, Role userRole);

    void changePassword(String username, ChangePasswordModel changePasswordModel);

    UserKeyModel getUserKey(String username);

}

