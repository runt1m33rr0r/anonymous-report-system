package com.primary.server.services;

import com.primary.server.entities.Role;

public interface JwtService {

    String generateToken(String username, Long userId, Role role);

}
