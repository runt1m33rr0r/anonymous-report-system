package com.primary.server.services.impl;

import com.primary.server.entities.ClientKeyBundle;
import com.primary.server.entities.Employee;
import com.primary.server.models.login.AdminLoginModel;
import com.primary.server.models.login.ClientLoginResultModel;
import com.primary.server.models.registration.UserRegistrationModel;
import com.primary.server.models.user.ChangePasswordModel;
import com.primary.server.models.user.UserKeyModel;
import com.primary.server.repositories.EmployeeRepository;
import com.primary.server.repositories.SignalKeyRepository;
import com.primary.server.repositories.UserRepository;
import com.primary.server.config.ModelMapper;
import com.primary.server.entities.User;
import com.primary.server.exceptions.ValidationException;
import com.primary.server.models.login.ClientLoginModel;
import com.primary.server.entities.Role;
import com.primary.server.services.JwtService;
import com.primary.server.services.UserService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Base64;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final SignalKeyRepository keyRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;
    private final JwtService jwtService;
    private EmployeeRepository employeeRepository;

    @Override
    @Transactional
    public ClientLoginResultModel login(ClientLoginModel loginModel) {
        this.validateUser(loginModel.getUsername(), loginModel.getPassword());

        User userEntity = this.userRepository.getFirstByUsername(loginModel.getUsername());
        ClientKeyBundle existingKey = userEntity.getClientKeyBundle();
        if (existingKey != null) {
            this.keyRepository.delete(existingKey);
        }

        Base64.Encoder encoder = Base64.getEncoder();
        ClientKeyBundle savedBundle = this.keyRepository.save(
                new ClientKeyBundle(
                        encoder.encodeToString(loginModel.getBasePublicKey()),
                        encoder.encodeToString(loginModel.getIdentityPublicKey()))
        );

        userEntity.setClientKeyBundle(savedBundle);
        this.userRepository.save(userEntity);

        ClientLoginResultModel loginResult = this.modelMapper.userEntityToClientLoginResult(userEntity);
        loginResult.setToken(this.jwtService.generateToken(
                userEntity.getUsername(),
                userEntity.getId(),
                userEntity.getRole()));

        return loginResult;
    }

    @Override
    public ClientLoginResultModel login(AdminLoginModel adminLogin) {
        this.validateUser(adminLogin.getUsername(), adminLogin.getPassword());

        User adminEntity = this.userRepository.getFirstByUsername(adminLogin.getUsername());
        ClientLoginResultModel loginResult = this.modelMapper.userEntityToClientLoginResult(adminEntity);
        loginResult.setToken(this.jwtService.generateToken(
                adminEntity.getUsername(),
                adminEntity.getId(),
                adminEntity.getRole()));

        return loginResult;
    }

    private void validateUser(String username, String password) {
        User userEntity = this.getUserEntity(username);
        if (!this.passwordEncoder.matches(password, userEntity.getPassword())) {
            throw new ValidationException("Wrong password!");
        }
    }

    @Override
    public void createUser(UserRegistrationModel registrationModel) {
        this.registerUser(registrationModel, Role.USER);
    }

    @Override
    @Transactional
    public User registerUser(UserRegistrationModel registrationModel, Role userRole) {
        if (this.userRepository.existsByUsername(registrationModel.getUsername())) {
            throw new ValidationException("Username taken");
        }

        String encodedPassword = this.passwordEncoder.encode(registrationModel.getPassword());
        User newUser = new User();
        newUser.setUsername(registrationModel.getUsername());
        newUser.setPassword(encodedPassword);
        newUser.setRole(userRole);

        return this.userRepository.save(newUser);
    }

    @Override
    @Transactional
    public void changePassword(String username, ChangePasswordModel changePasswordModel) {
        User userEntity = this.getUserEntity(username);
        String encodedPassword = this.passwordEncoder.encode(changePasswordModel.getPassword());
        userEntity.setPassword(encodedPassword);
        this.userRepository.save(userEntity);
    }

    @Override
    public UserKeyModel getUserKey(String username) {
        User userEntity = this.getUserEntity(username);
        Base64.Decoder decoder = Base64.getDecoder();

        return new UserKeyModel(
                decoder.decode(userEntity.getClientKeyBundle().getBasePublicKey()),
                decoder.decode(userEntity.getClientKeyBundle().getIdentityPublicKey()));
    }

    private User getUserEntity(String username) {
        User userEntity = this.userRepository.getFirstByUsername(username);
        if (userEntity == null) {
            throw new ValidationException("User does not exist");
        }

        return userEntity;
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (this.userRepository.countUsersByRole(Role.ADMIN) == 0) {
            this.registerUser(
                    new UserRegistrationModel("admin", "123456"),
                    Role.ADMIN);
        }

        if (this.userRepository.countUsersByRole(Role.USER) == 0) {
            this.registerUser(
                    new UserRegistrationModel("user", "123456"),
                    Role.USER);
        }

        if (this.userRepository.countUsersByRole(Role.EMPLOYEE) == 0) {
            User newUser = this.registerUser(
                    new UserRegistrationModel("empl", "123456"),
                    Role.EMPLOYEE);

            this.employeeRepository.save(new Employee(newUser, new ArrayList<>()));
        }
    }

}
