package com.primary.server.services.impl;

import com.primary.server.entities.*;
import com.primary.server.exceptions.ValidationException;
import com.primary.server.models.comments.CommentModel;
import com.primary.server.models.comments.CreateCommentModel;
import com.primary.server.repositories.CommentRepository;
import com.primary.server.repositories.ReportRepository;
import com.primary.server.repositories.UserRepository;
import com.primary.server.services.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository;
    private ReportRepository reportRepository;
    private UserRepository userRepository;

    @Override
    public CommentModel createComment(CreateCommentModel commentModel, String author, Role authorRole) {
        this.validateUser(
                author,
                authorRole,
                commentModel.getReportId(),
                MessageFormat.format("{0} is not allowed to post comments", author));

        User commentAuthor = this.userRepository.getFirstByUsername(author);
        Optional<Report> commentReport = this.reportRepository.findById(commentModel.getReportId());
        if (commentReport.isEmpty()) {
            throw new ValidationException("Report does not exist");
        }

        Comment newComment = this.commentRepository.save(new Comment(
                commentAuthor,
                commentReport.get(),
                commentModel.getContent(),
                commentModel.getContentCopy(),
                LocalDateTime.now(),
                commentModel.getAttachedFileName()));

        return new CommentModel(
                newComment.getId(),
                newComment.getAuthor().getUsername(),
                newComment.getContentCopy(),
                newComment.getCreationTime(),
                newComment.getAttachedFileName());
    }

    @Override
    public List<CommentModel> getComments(Long reportId, String username, Role userRole) {
        this.validateUser(
                username,
                userRole,
                reportId,
                MessageFormat.format("{0} is not allowed to view these comments", username));

        return this.commentRepository.findAllByReportIdOrderByCreationTime(reportId)
                .stream()
                .map(comment -> {
                    if (comment.getAuthor().getUsername().equals(username)) {
                        return new CommentModel(
                                comment.getId(),
                                comment.getAuthor().getUsername(),
                                comment.getContentCopy(),
                                comment.getCreationTime(),
                                comment.getAttachedFileName());
                    } else {
                        return new CommentModel(
                                comment.getId(),
                                comment.getAuthor().getUsername(),
                                comment.getContent(),
                                comment.getCreationTime(),
                                comment.getAttachedFileName());
                    }
                })
                .collect(Collectors.toList());
    }

    private void validateUser(String author, Role authorRole, Long reportId, String errorMessage) {
        if (authorRole == Role.ADMIN) {
            throw new ValidationException(errorMessage);
        }

        Optional<Report> report = this.reportRepository.findById(reportId);
        if (report.isEmpty()) {
            throw new ValidationException("Report does not exist");
        }

        if (authorRole == Role.USER && !report.get().getAuthor().getUsername().equals(author)) {
            throw new ValidationException(errorMessage);
        }

        if (authorRole == Role.EMPLOYEE && !report.get().getAgent().getUser().getUsername().equals(author)) {
            throw new ValidationException(errorMessage);
        }
    }

}
