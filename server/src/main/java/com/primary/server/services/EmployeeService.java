package com.primary.server.services;

import com.primary.server.models.employees.EmployeeContactModel;
import com.primary.server.models.registration.UserRegistrationModel;

public interface EmployeeService {

    EmployeeContactModel getEmployee();

    void createEmployee(UserRegistrationModel registrationModel);

}
