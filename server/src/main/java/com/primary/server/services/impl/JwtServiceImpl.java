package com.primary.server.services.impl;

import com.primary.server.entities.Role;
import com.primary.server.services.JwtService;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;

@Service
public class JwtServiceImpl implements JwtService {

    private static final String KEY_STRING = """
				secret
				secret
				secret
				secret
				secret
				secret
				secret
				""";
    private static final SecretKey SECRET = Keys.hmacShaKeyFor(KEY_STRING.getBytes());

	@Override
    public String generateToken(String username, Long userId, Role role) {
        // 15 minutes
        long EXPIRATION_TIME = 900_000;
        return Jwts.builder()
				.subject(username)
                .claim("role", role)
				.claim("id", String.valueOf(userId))
                .issuedAt(new Date(System.currentTimeMillis()))
                .expiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SECRET)
                .compact();
    }

}