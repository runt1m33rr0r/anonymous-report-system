package com.primary.server.services.impl;

import com.primary.server.entities.Employee;
import com.primary.server.entities.Role;
import com.primary.server.entities.User;
import com.primary.server.exceptions.ValidationException;
import com.primary.server.models.employees.EmployeeContactModel;
import com.primary.server.models.registration.UserRegistrationModel;
import com.primary.server.repositories.EmployeeRepository;
import com.primary.server.services.EmployeeService;
import com.primary.server.services.UserService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeRepository employeeRepository;
    private UserService userService;

    @Override
    public EmployeeContactModel getEmployee() {
        List<Employee> agents = this.employeeRepository.findByLeastAssignedReports(
                PageRequest.of(0, 1));
        if (agents.isEmpty()) {
            throw new ValidationException("Could not find suitable agent");
        }

        return new EmployeeContactModel(agents.get(0).getUser().getUsername());
    }

    @Override
    @Transactional
    public void createEmployee(UserRegistrationModel registrationModel) {
        User registeredUser = this.userService.registerUser(registrationModel, Role.EMPLOYEE);
        Employee employee = new Employee(registeredUser, new ArrayList<>());

        this.employeeRepository.save(employee);
    }

}
