package com.primary.server.config;

import com.primary.server.entities.User;
import com.primary.server.models.login.ClientLoginResultModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ModelMapper {

    @Mapping(source = "username", target = "username")
    @Mapping(source = "role", target = "role")
    ClientLoginResultModel userEntityToClientLoginResult(User userEntity);

}