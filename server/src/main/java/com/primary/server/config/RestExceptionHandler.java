package com.primary.server.config;

import com.primary.server.exceptions.AuthenticationException;
import com.primary.server.exceptions.EncryptionException;
import com.primary.server.exceptions.ValidationException;
import com.primary.server.models.ErrorModel;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({
            AuthenticationException.class,
            EncryptionException.class,
            ValidationException.class})
    protected ResponseEntity<Object> handleExceptions(Exception ex) {
        return new ResponseEntity<>(new ErrorModel(ex.getMessage()), HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NonNull MethodArgumentNotValidException ex,
                                                                  @NonNull HttpHeaders headers,
                                                                  @NonNull HttpStatusCode status,
                                                                  @NonNull WebRequest request) {
        StringBuilder errorMessageBuilder = new StringBuilder();
        for (final ObjectError error : ex.getBindingResult().getAllErrors()) {
            if (!errorMessageBuilder.isEmpty()) {
                errorMessageBuilder.append(" ");
            }

            errorMessageBuilder.append(error.getDefaultMessage());
        }

        return handleExceptionInternal(
                ex,
                new ErrorModel(errorMessageBuilder.toString()),
                headers,
                HttpStatus.OK,
                request);
    }

}