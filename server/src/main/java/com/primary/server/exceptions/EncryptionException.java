package com.primary.server.exceptions;

public class EncryptionException extends RuntimeException {
    public EncryptionException(String message) {
        super(message);
    }
}