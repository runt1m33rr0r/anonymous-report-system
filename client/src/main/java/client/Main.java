package client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import client.utils.UserInterface;

import java.io.*;
import java.util.Map;

public class Main extends Application {

    @Override
    public void start(Stage stage) {
        Application thisApplication = this;

        Platform.runLater(() -> {
            try {
                UserInterface.initialize(thisApplication, stage);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static void main(String[] args) {
        // Network.initSession();

//        talkWithServer();
//        talkWithServer();

        //initTor();
        launch(args);
    }

    private static void initTor() throws IOException {
        String torPath = "/home/user/Плот/tor-browser_en-US/Browser/TorBrowser/Tor/tor";
        ProcessBuilder builder = new ProcessBuilder(torPath);
        builder.inheritIO();
        Map<String, String> env = builder.environment();
        env.put("LD_LIBRARY_PATH", "/home/user/Плот/tor-browser_en-US/Browser/TorBrowser/Tor");
        Process process = builder.start();

        Runtime.getRuntime().addShutdownHook(new Thread(process::destroy));
    }

//    private static void talkWithServer() throws Exception {
//        TestModel dataToSend = new TestModel("hey there");
//        TestModel receivedData = makeEncryptedPostRequest("http://localhost:8081/", dataToSend, TestModel.class);
//
//        System.out.println(receivedData.getContent());
//    }
}
