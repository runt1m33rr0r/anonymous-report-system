package client.models;

public enum Role {
    ADMIN, EMPLOYEE, USER
}