package client.models.comments;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateCommentModel {

    private Long reportId;
    private String content;
    private String contentCopy;
    private String attachedFileName;

}
