package client.models.encryption;

import client.models.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientLoginResultModel {
    private String username;
    private Role role;
    private String token;
}