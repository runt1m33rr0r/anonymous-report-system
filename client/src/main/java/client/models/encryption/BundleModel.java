package client.models.encryption;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BundleModel {
    private String username;
    private byte[] basePublicKey;
    private byte[] identityPublicKey;
}