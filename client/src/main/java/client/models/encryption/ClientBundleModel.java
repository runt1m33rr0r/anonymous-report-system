package client.models.encryption;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientBundleModel {
    private String username;
    private String password;
    private byte[] basePublicKey;
    private byte[] identityPublicKey;
}