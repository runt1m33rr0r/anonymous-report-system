package client.models.encryption;

public enum RequestMethod {
    GET,
    POST,
    PUT,
    DELETE
}