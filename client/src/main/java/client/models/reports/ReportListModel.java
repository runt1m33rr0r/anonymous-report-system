package client.models.reports;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReportListModel {

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReportModel {
        private Long id;
        private String author;
        private String agent;
        private String title;
        private LocalDate creationDate;
        private ReportStatus status;
    }

    private List<ReportModel> reports;

    private int pagesCount;

}