package client.models.reports;

public enum ReportStatus {
    OPEN, IN_PROGRESS, CLOSED
}