package client.models.reports;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateReportModel {
    private String title;
    private String titleCopy;
    private String content;
    private String contentCopy;
    private String agentName;
}