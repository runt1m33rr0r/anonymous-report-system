package client.services;

import client.exceptions.RequestException;
import client.models.*;
import client.models.encryption.ClientBundleModel;

import client.models.encryption.ClientLoginResultModel;
import client.models.users.AdminLoginModel;
import client.models.users.UserRegistrationModel;
import client.utils.*;
import client.utils.serialization.UserKeySerializable;

public class UserService {

    public static void loginUser(String username, String password) throws Exception {
        UserKeySerializable keys = LocalStorage.getUserKey(username);
        byte[] clientBasePublicKey = keys.getClientBasePublicKey();
        byte[] clientIdentityPublicKey = keys.getClientIdentityPublicKey();

        ClientBundleModel clientBundle = new ClientBundleModel(
                username,
                password,
                clientBasePublicKey,
                clientIdentityPublicKey);

       ClientLoginResultModel result = Network.makePostRequest(
                "/login",
                clientBundle,
                ClientLoginResultModel.class);

       Network.setClientName(result.getUsername());
       Network.setClientRole(result.getRole());
       Network.setToken(result.getToken());
       Security.initializeSignalStore();
    }

    public static void loginAdmin(String username, String password) throws RequestException {
        ClientLoginResultModel loginResult = Network.makePostRequest(
                "/admin",
                new AdminLoginModel(username, password),
                ClientLoginResultModel.class
        );

        Network.setClientName(loginResult.getUsername());
        Network.setClientRole(loginResult.getRole());
        Network.setToken(loginResult.getToken());
    }

    public static void changePassword(String newPassword) throws RequestException {
        UpdatePasswordModel updatePasswordModel = new UpdatePasswordModel(newPassword);
        Network.makePutRequest("/password", updatePasswordModel);
    }

    public static void createEmployee(String username, String password) throws RequestException {
        Network.makePostRequest("/employees", new UserRegistrationModel(username, password));
    }

    public static void createUser(String username, String password) throws RequestException {
        Network.makePostRequest("/register", new UserRegistrationModel(username, password));
    }

}
