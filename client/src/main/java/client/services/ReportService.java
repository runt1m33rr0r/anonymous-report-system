package client.services;

import client.exceptions.EncryptionException;
import client.exceptions.RequestException;
import client.models.Role;
import client.models.comments.CommentModel;
import client.models.comments.CreateCommentModel;
import client.models.employees.EmployeeContactModel;
import client.models.reports.CreateReportModel;
import client.models.reports.ReportListModel;
import client.models.reports.ReportStatus;
import client.utils.Network;
import client.utils.ProgressTrackingInputStream;
import client.utils.Security;
import client.utils.UserInterface;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.util.Pair;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.springframework.core.io.InputStreamResource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.function.Consumer;

public class ReportService {

    public static void createReport(String title, String description)
            throws RequestException, EncryptionException {
        EmployeeContactModel employee = Network.makeGetRequest("/employees", EmployeeContactModel.class);
        SignalProtocolAddress employeeAddress = new SignalProtocolAddress(employee.getName(), 1);
        String encryptedTitle = Security.encryptMessage(employeeAddress, title);
        String encryptedTitleForSelf = Security.encryptMessage(Security.getSelfAddress(), title);
        String encryptedDescription = Security.encryptMessage(employeeAddress, description);
        String encryptedDescriptionForSelf = Security.encryptMessage(Security.getSelfAddress(), description);

        Network.makePostRequest("/reports", new CreateReportModel(
                encryptedTitle,
                encryptedTitleForSelf,
                encryptedDescription,
                encryptedDescriptionForSelf,
                employee.getName()));
    }

    public static CommentModel createComment(long reportId,
                                             String content,
                                             String reportAuthorName,
                                             String reportAgentName)
            throws EncryptionException, RequestException {
        Role userRole = Network.getClientRole();
        if (userRole == Role.ADMIN) {
            return null;
        }

        String recipient = userRole == Role.USER ? reportAgentName : reportAuthorName;
        SignalProtocolAddress recipientAddress = new SignalProtocolAddress(recipient, 1);
        String encryptedContent = Security.encryptMessage(recipientAddress, content);
        String encryptedSelfContent = Security.encryptMessage(Security.getSelfAddress(), content);

        CommentModel resultComment = Network.makePostRequest(
                "/comments",
                new CreateCommentModel(
                        reportId,
                        encryptedContent,
                        encryptedSelfContent,
                        null),
                CommentModel.class);
        resultComment.setContent(content);

        return resultComment;
    }

    public static void attachFile(long reportId,
                                  String content,
                                  String reportAuthorName,
                                  String reportAgentName,
                                  File file,
                                  Consumer<CommentModel> commentResult) throws EncryptionException {
        Role userRole = Network.getClientRole();
        if (userRole == Role.ADMIN) {
            return;
        }

        String recipient = userRole == Role.USER ? reportAgentName : reportAuthorName;
        SignalProtocolAddress recipientAddress = new SignalProtocolAddress(recipient, 1);
        String encryptedContent = Security.encryptMessage(recipientAddress, content);
        String encryptedSelfContent = Security.encryptMessage(Security.getSelfAddress(), content);

        File selfEncryptedFile = createEncryptionTempFile(file.getName(), Network.getClientName());
        File recipientEncryptedFile = createEncryptionTempFile(file.getName(), recipient);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        Task<CommentModel> task = new Task<>() {
            @Override
            protected CommentModel call() throws RequestException, FileNotFoundException, EncryptionException {
                this.updateTitle("Encrypting " + file.getName());
                this.updateProgress(0, 1);

                encryptFile(
                        file,
                        selfEncryptedFile,
                        Security.getSelfAddress(),
                        progress -> this.updateProgress(progress / 4, 1));
                encryptFile(
                        file,
                        recipientEncryptedFile,
                        recipientAddress,
                        progress -> this.updateProgress(progress / 4 + 0.25, 1));

                this.updateTitle("Uploading " + file.getName());
                try {
                    body.add("files", getUploadResource(
                            selfEncryptedFile,
                            progress -> this.updateProgress(progress / 4 + 0.5, 1)));
                    body.add("files", getUploadResource(
                            recipientEncryptedFile,
                            progress -> this.updateProgress(progress / 4 + 0.75, 1)));
                } catch (FileNotFoundException e) {
                    throw new FileNotFoundException("Could not find file for upload");
                }

                body.add("comment", new CreateCommentModel(
                        reportId,
                        encryptedContent,
                        encryptedSelfContent,
                        file.getName()));
                CommentModel resultComment = Network.makePostRequest(
                        "/files",
                        body,
                        CommentModel.class);
                resultComment.setContent(content);

                return resultComment;
            }
        };

        Runnable deleteTempFiles = () -> {
            String errorTitle = "Upload cancel error";
            String errorMessage = "Could not delete upload temp file";
            if (selfEncryptedFile.exists() && !selfEncryptedFile.delete()) {
                UserInterface.showDialogMessage(errorTitle, errorMessage);
            }

            if (recipientEncryptedFile.exists() && !recipientEncryptedFile.delete()) {
                UserInterface.showDialogMessage(errorTitle, errorMessage);
            }
        };
        Pair<Alert, ProgressBar> progress = UserInterface.showProgressMessage(
                "Uploading " + file.getName(),
                () -> {
                    task.cancel();
                    deleteTempFiles.run();
                });
        progress.getValue().progressProperty().bind(task.progressProperty());
        progress.getKey().titleProperty().bind(task.titleProperty());
        task.setOnSucceeded(event -> {
            deleteTempFiles.run();
            UserInterface.closeMessageAsync(progress.getKey());
            commentResult.accept(task.getValue());
        });
        task.setOnCancelled(event -> {
            deleteTempFiles.run();
            UserInterface.closeMessageAsync(progress.getKey());
        });
        task.setOnFailed(event -> {
            deleteTempFiles.run();
            UserInterface.closeMessageAsync(progress.getKey());

            Throwable failure = task.getException();
            if (failure != null) {
                UserInterface.showDialogMessage("Upload error", failure.getMessage());
            }
        });
        new Thread(task).start();
    }

    public static void downloadFile(long reportId, String fileAuthorName, String fileName, File saveLocation) {
        if (!saveLocation.exists()) {
            UserInterface.showDialogMessage("Filesystem error", "%s does not exist".formatted(saveLocation));
            return;
        }

        File outputFile = Paths.get(saveLocation.getPath(), fileName).toFile();
        File decryptionTempFile = Paths.get(outputFile.getPath() + ".temp").toFile();
        Task<Void> task = new Task<>() {
            @Override
            protected Void call() throws EncryptionException, IOException {
                this.updateTitle("Downloading " + fileName);
                this.updateProgress(0, 1);
                Network.makeDownloadRequest(MessageFormat.format(
                                "files/{0}/{1}",
                                String.valueOf(reportId),
                                fileName))
                        .exchange((request, response) -> {
                            long fileSize = response.getHeaders().getContentLength();
                            long byteCount = 0;
                            try (InputStream inputStream = response.getBody();
                                 FileOutputStream outputStream = new FileOutputStream(outputFile)) {
                                byte[] buffer = new byte[8192]; // 8 KB buffer
                                int bytesRead;
                                while ((bytesRead = inputStream.read(buffer)) != -1) {
                                    outputStream.write(buffer, 0, bytesRead);
                                    byteCount += bytesRead;
                                    this.updateProgress((double) byteCount / fileSize / 2, 1);
                                }
                            }

                            return null;
                        });

                this.updateTitle("Decrypting " + fileName);
                decryptFile(
                        outputFile,
                        decryptionTempFile,
                        fileAuthorName,
                        progress -> this.updateProgress(progress / 2 + 0.5, 1));

                return null;
            }
        };

        Runnable deleteTempFile = () -> {
            if (decryptionTempFile.exists() && !decryptionTempFile.delete()) {
                UserInterface.showDialogMessage(
                        "Decryption cancel error",
                        "Could not delete partially decrypted file");
            }
        };
        Pair<Alert, ProgressBar> progress = UserInterface.showProgressMessage(
                "Downloading " + fileName,
                () -> {
                    task.cancel();
                    deleteTempFile.run();

                    if (outputFile.exists() && !outputFile.delete()) {
                        UserInterface.showDialogMessage(
                                "Download cancel error",
                                "Could not delete partially downloaded file");
                    }
                });
        progress.getValue().progressProperty().bind(task.progressProperty());
        progress.getKey().titleProperty().bind(task.titleProperty());
        task.setOnSucceeded(event -> {
            deleteTempFile.run();
            UserInterface.closeMessageAsync(progress.getKey());
        });
        task.setOnCancelled(event -> {
            deleteTempFile.run();
            UserInterface.closeMessageAsync(progress.getKey());
        });
        task.setOnFailed(event -> {
            deleteTempFile.run();
            UserInterface.closeMessageAsync(progress.getKey());

            Throwable failure = task.getException();
            if (failure != null) {
                UserInterface.showDialogMessage("Download error", failure.getMessage());
            }
        });
        new Thread(task).start();
    }

    private static InputStreamResource getUploadResource(File file, Consumer<Double> progressCallback)
            throws FileNotFoundException {
        long totalBytes = file.length();
        FileInputStream fis = new FileInputStream(file);
        ProgressTrackingInputStream trackingInputStream = new ProgressTrackingInputStream(
                fis,
                totalBytes,
                progressCallback);

        return new InputStreamResource(trackingInputStream) {
            @Override
            public long contentLength() {
                return totalBytes;
            }

            @Override
            public String getFilename() {
                return file.getName();
            }
        };
    }

    private static void encryptFile(File inputFile,
                                    File outputFile,
                                    SignalProtocolAddress recipient,
                                    Consumer<Double> progressCallback) throws EncryptionException {
        final long totalBytes = inputFile.length();
        long bytesEncrypted = 0;
        int chunkSize = 1024 * 1000; // 1 MB
        try (FileInputStream input = new FileInputStream(inputFile);
             PrintWriter output = new PrintWriter(outputFile)) {
            byte[] buffer = new byte[chunkSize];
            int bytesRead;
            while ((bytesRead = input.read(buffer)) != -1) {
                byte[] chunk = new byte[bytesRead];
                System.arraycopy(buffer, 0, chunk, 0, chunk.length);
                String encrypted;
                encrypted = Security.encryptBytes(
                        Objects.requireNonNullElseGet(recipient, Security::getSelfAddress),
                        chunk);
                output.println(encrypted);

                bytesEncrypted += bytesRead;
                progressCallback.accept((double) bytesEncrypted / totalBytes);
            }
        } catch (IOException e) {
            throw new EncryptionException("File error while encrypting");
        }
    }

    private static void decryptFile(File inputFile,
                                    File tempFile,
                                    String fileAuthor,
                                    Consumer<Double> progressCallback) throws EncryptionException, IOException {
        final long totalBytes = inputFile.length();
        long bytesRead = 0;
        try (Scanner input = new Scanner(inputFile)) {
            try (FileOutputStream output = new FileOutputStream(tempFile, true)) {
                while (input.hasNextLine()) {
                    String line = input.nextLine();
                    byte[] decrypted;
                    if (fileAuthor.equals(Network.getClientName())) {
                        decrypted = Security.decryptSelfMessageToBytes(line);
                    } else {
                        decrypted = Security.decryptUserMessageToBytes(fileAuthor, line);
                    }

                    output.write(decrypted);

                    bytesRead += line.getBytes().length;
                    progressCallback.accept((double) bytesRead / totalBytes);
                }
            } catch (FileNotFoundException e) {
                throw new FileNotFoundException("Decryption output file not found");
            } catch (IOException e) {
                throw new IOException("Error while writing decrypted data");
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Decryption input file not found");
        }

        if (!inputFile.delete()) {
            throw new IOException("Could not delete unencrypted file " + inputFile.getName());
        }

        if (!tempFile.renameTo(inputFile)) {
            throw new IOException("Could not rename temporary decrypted file " + inputFile.getName());
        }
    }

    private static File createEncryptionTempFile(String sourceFileName, String recipient) throws EncryptionException {
        Path tempDirectory = Paths.get(System.getProperty("java.io.tmpdir"), "report-system", recipient);
        if (Files.notExists(tempDirectory)) {
            try {
                Files.createDirectories(tempDirectory);
            } catch (IOException e) {
                throw new EncryptionException("Could not create temporary encryption directory");
            }
        }

        File outputFile = Paths.get(tempDirectory.toFile().getPath(), sourceFileName).toFile();
        if (outputFile.exists()) {
            if (!outputFile.delete()) {
                throw new EncryptionException("Could not delete previous temp file upload attempt");
            }
        }

        return outputFile;
    }

    public static List<CommentModel> getReportComments(ReportListModel.ReportModel reportModel)
            throws RequestException {
        Role userRole = Network.getClientRole();
        if (userRole == Role.ADMIN) {
            return null;
        }

        List<CommentModel> comments = List.of(Network.makeGetRequest(
                "/comments?reportId=" + reportModel.getId(),
                CommentModel[].class));
        for (CommentModel comment : comments) {
            comment.setContent(decryptCommentContent(comment));
        }

        return comments;
    }

    public static ReportListModel getReports(ReportStatus status, int page)
            throws RequestException, EncryptionException {
        Role userRole = Network.getClientRole();
        if (userRole == Role.ADMIN) {
            return null;
        }

        ReportListModel reports = Network.makeGetRequest(
                "/reports?status=" + status + "&page=" + page,
                ReportListModel.class);
        for (ReportListModel.ReportModel report : reports.getReports()) {
            report.setTitle(decryptReportTitle(report, userRole));
        }

        return reports;
    }

    private static String decryptReportTitle(ReportListModel.ReportModel report, Role userRole)
            throws EncryptionException {
        String title;
        if (userRole == Role.USER) {
            title = Security.decryptSelfMessage(report.getTitle());
        } else {
            title = Security.decryptUserMessage(report.getAuthor(), report.getTitle());
        }

        return title;
    }

    private static String decryptCommentContent(CommentModel comment) {
        if (Network.getClientRole() == Role.ADMIN) {
            return "";
        }

        String currentUser = Network.getClientName();
        String content;
        try {
            if (comment.getAuthor().equals(currentUser)) {
                content = Security.decryptSelfMessage(comment.getContent());
            } else {
                content = Security.decryptUserMessage(comment.getAuthor(), comment.getContent());
            }
        } catch (EncryptionException e) {
            return e.getMessage();
        }

        return content;
    }

}
