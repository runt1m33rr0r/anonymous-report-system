package client.utils;

import org.springframework.lang.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

public class ProgressTrackingInputStream extends InputStream {

    private final InputStream delegate;
    private final long totalBytes;
    private long bytesRead = 0;
    private final Consumer<Double> progressCallback;

    public ProgressTrackingInputStream(InputStream delegate, long totalBytes, Consumer<Double> progressCallback) {
        this.delegate = delegate;
        this.totalBytes = totalBytes;
        this.progressCallback = progressCallback;
    }

    @Override
    public int read() throws IOException {
        int byteRead = delegate.read();
        if (byteRead != -1) {
            bytesRead++;
            reportProgress();
        }

        return byteRead;
    }

    @Override
    public int read(@NonNull byte[] bytes, int off, int len) throws IOException {
        int bytesReadNow = delegate.read(bytes, off, len);
        if (bytesReadNow > 0) {
            bytesRead += bytesReadNow;
            reportProgress();
        }

        return bytesReadNow;
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }

    private void reportProgress() {
        double progress = (double) bytesRead / totalBytes;
        progressCallback.accept(progress);
    }

}
