package client.utils;

import client.utils.serialization.UserKeySerializable;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;

import java.io.*;
import java.text.MessageFormat;

public class LocalStorage {

    private static final String STORAGE_DIRECTORY = "storage";

    public static UserKeySerializable getUserKey(String username) {
        if (!userKeysExist(username)) {
            generateUserKeys(username);
        }

        return (UserKeySerializable) readData(getUserKeyFilePath(username));
    }

    private static void generateUserKeys(String username) {
        if (userKeysExist(username)) {
            return;
        }

        ECKeyPair clientIdentityKeyPair = Curve.generateKeyPair();
        ECKeyPair clientBaseKey = Curve.generateKeyPair();
        byte[] clientIdentityPublicKey = clientIdentityKeyPair.getPublicKey().serialize();
        byte[] clientIdentityPrivateKey = clientIdentityKeyPair.getPrivateKey().serialize();
        byte[] clientBasePublicKey = clientBaseKey.getPublicKey().serialize();
        byte[] clientBasePrivateKey = clientBaseKey.getPrivateKey().serialize();
        UserKeySerializable userKeySerializable = new UserKeySerializable(
                clientIdentityPublicKey,
                clientIdentityPrivateKey,
                clientBasePublicKey,
                clientBasePrivateKey
        );

        saveData(userKeySerializable, getUserKeyFilePath(username));
    }

    private static boolean userKeysExist(String keyFileName) {
        return dataExists(getUserKeyFilePath(keyFileName));
    }

    private static String getUserKeyFilePath(String username) {
        return STORAGE_DIRECTORY + "/" + username + ".data";
    }

    private static void saveData(Serializable data, String filePath) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Object readData(String filePath) {
        if (!dataExists(filePath)) {
            throw new RuntimeException(MessageFormat.format(
                    "Data at {0} does not exist",
                    filePath));
        }

        try (FileInputStream fileInputStream = new FileInputStream(filePath);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {

            return objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean dataExists(String filePath) {
        File keyFile = new File(filePath);

        return keyFile.exists() && keyFile.isFile();
    }

}