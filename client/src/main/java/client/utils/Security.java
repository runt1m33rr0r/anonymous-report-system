package client.utils;

import client.exceptions.EncryptionException;
import client.exceptions.RequestException;
import client.models.Role;
import client.models.users.UserKeyModel;
import client.utils.serialization.UserKeySerializable;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.protocol.*;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPrivateKey;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.message.CiphertextMessage;
import org.signal.libsignal.protocol.message.SignalMessage;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.signal.libsignal.protocol.state.SignalProtocolStore;
import org.signal.libsignal.protocol.state.impl.InMemorySignalProtocolStore;
import org.signal.libsignal.protocol.util.KeyHelper;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class Security {

    private static final Map<SignalProtocolAddress, UserKeyModel> aliceKeyCache = new HashMap<>();
    private static final Map<SignalProtocolAddress, UserKeyModel> bobKeyCache = new HashMap<>();

    private static SignalProtocolStore signalStore;

    public static SessionRecord buildAliceSession(byte[] aliceIdentityPublicKey,
                                                  byte[] aliceIdentityPrivateKey,
                                                  byte[] aliceBasePublicKey,
                                                  byte[] aliceBasePrivateKey,
                                                  byte[] bobIdentityPublicKey,
                                                  byte[] bobBasePublicKey) throws EncryptionException {
        try {
            IdentityKeyPair sourceIdentityPair = new IdentityKeyPair(
                    new IdentityKey(aliceIdentityPublicKey),
                    new ECPrivateKey(Native.ECPrivateKey_Deserialize(aliceIdentityPrivateKey))
            );
            ECKeyPair sourceBaseKey = new ECKeyPair(
                    new ECPublicKey(aliceBasePublicKey),
                    new ECPrivateKey(Native.ECPrivateKey_Deserialize(aliceBasePrivateKey))
            );

            return SessionRecord.initializeAliceSession(
                    sourceIdentityPair,
                    sourceBaseKey,
                    new IdentityKey(bobIdentityPublicKey, 0),
                    new ECPublicKey(bobBasePublicKey),
                    new ECPublicKey(bobBasePublicKey)
            );
        } catch (Exception e) {
            throw new EncryptionException(e.getMessage());
        }
    }

    public static SessionRecord buildBobSession(byte[] bobIdentityPublicKey,
                                                byte[] bobIdentityPrivateKey,
                                                byte[] bobBasePublicKey,
                                                byte[] bobBasePrivateKey,
                                                byte[] aliceIdentityPublicKey,
                                                byte[] aliceBasePublicKey) throws EncryptionException {
        try {
            IdentityKeyPair sourceIdentityPair = new IdentityKeyPair(
                    new IdentityKey(bobIdentityPublicKey),
                    new ECPrivateKey(Native.ECPrivateKey_Deserialize(bobIdentityPrivateKey))
            );
            ECKeyPair sourceBaseKey = new ECKeyPair(
                    new ECPublicKey(bobBasePublicKey),
                    new ECPrivateKey(Native.ECPrivateKey_Deserialize(bobBasePrivateKey))
            );

            return SessionRecord.initializeBobSession(
                    sourceIdentityPair,
                    sourceBaseKey,
                    sourceBaseKey,
                    new IdentityKey(aliceIdentityPublicKey, 0),
                    new ECPublicKey(aliceBasePublicKey)
            );
        } catch (Exception e) {
            throw new EncryptionException(e.getMessage());
        }
    }

    public static void initializeSignalStore() throws EncryptionException {
        initializeSignalStore(null, false);
    }

    public static void initializeSignalStore(SignalProtocolAddress initAddress,
                                             boolean isAlice) throws EncryptionException {
        if (Network.getClientRole() == Role.ADMIN) {
            return;
        }

        UserKeySerializable myKey = LocalStorage.getUserKey(Network.getClientName());

        byte[] clientIdentityPublicKey = myKey.getClientIdentityPublicKey();
        IdentityKey clientIdentityPublicKeyObject;
        try {
            clientIdentityPublicKeyObject = new IdentityKey(clientIdentityPublicKey);
        } catch (InvalidKeyException e) {
            throw new EncryptionException(e.getMessage());
        }

        byte[] clientIdentityPrivateKey = myKey.getClientIdentityPrivateKey();
        IdentityKeyPair clientIdentityPair;
        try {
            clientIdentityPair = new IdentityKeyPair(
                    clientIdentityPublicKeyObject,
                    new ECPrivateKey(Native.ECPrivateKey_Deserialize(clientIdentityPrivateKey))
            );
        } catch (Exception ex) {
            throw new EncryptionException(ex.getMessage());
        }

        signalStore = new InMemorySignalProtocolStore(
                clientIdentityPair,
                KeyHelper.generateRegistrationId(false)
        );

        SessionRecord clientEncryptionSession = buildAliceSession(
                myKey.getClientIdentityPublicKey(),
                myKey.getClientIdentityPrivateKey(),
                myKey.getClientBasePublicKey(),
                myKey.getClientBasePrivateKey(),
                myKey.getClientIdentityPublicKey(),
                myKey.getClientBasePublicKey()
        );
        SessionRecord clientDecryptionSession = buildBobSession(
                myKey.getClientIdentityPublicKey(),
                myKey.getClientIdentityPrivateKey(),
                myKey.getClientBasePublicKey(),
                myKey.getClientBasePrivateKey(),
                myKey.getClientIdentityPublicKey(),
                myKey.getClientBasePublicKey()
        );

        signalStore.storeSession(getAddress(), clientEncryptionSession);
        signalStore.storeSession(getSelfAddress(), clientDecryptionSession);

        if (initAddress == null) {
            return;
        }

        UserKeyModel initKey;
        if (isAlice) {
            initKey = aliceKeyCache.get(initAddress);
            if (initKey == null) {
                initKey = getUserKey(initAddress.getName());
                aliceKeyCache.put(initAddress, initKey);
            }

            signalStore.storeSession(initAddress, buildAliceSession(
                    myKey.getClientIdentityPublicKey(),
                    myKey.getClientIdentityPrivateKey(),
                    myKey.getClientBasePublicKey(),
                    myKey.getClientBasePrivateKey(),
                    initKey.getIdentityPublicKey(),
                    initKey.getBasePublicKey()
            ));
        } else {
            initKey = bobKeyCache.get(initAddress);
            if (initKey == null) {
                initKey = getUserKey(initAddress.getName());
                bobKeyCache.put(initAddress, initKey);
            }

            signalStore.storeSession(initAddress, buildBobSession(
                    myKey.getClientIdentityPublicKey(),
                    myKey.getClientIdentityPrivateKey(),
                    myKey.getClientBasePublicKey(),
                    myKey.getClientBasePrivateKey(),
                    initKey.getIdentityPublicKey(),
                    initKey.getBasePublicKey()));
        }
    }

    public static String encryptMessage(SignalProtocolAddress destinationAddress, String message)
            throws EncryptionException {
        return encryptBytes(destinationAddress, message.getBytes());
    }

    public static String encryptBytes(SignalProtocolAddress destinationAddress, byte[] data)
            throws EncryptionException {
        initializeSignalStore(destinationAddress, true);

        SessionCipher clientSessionCipher = new SessionCipher(signalStore, destinationAddress);
        CiphertextMessage messageForServer;
        try {
            messageForServer = clientSessionCipher.encrypt(data);
        } catch (NoSessionException | UntrustedIdentityException e) {
            throw new EncryptionException(e.getMessage());
        }

        return Base64.getEncoder().encodeToString(messageForServer.serialize());
    }

    public static String decryptSelfMessage(String message) throws EncryptionException {
        return new String(decryptSelfMessageToBytes(message));
    }

    public static byte[] decryptSelfMessageToBytes(String message) throws EncryptionException {
        initializeSignalStore();

        return decryptMessage(getSelfAddress(), message);
    }

    public static String decryptUserMessage(String authorName, String message) throws EncryptionException {
        return new String(decryptUserMessageToBytes(authorName, message));
    }

    public static byte[] decryptUserMessageToBytes(String authorName, String message) throws EncryptionException {
        SignalProtocolAddress authorAddress = new SignalProtocolAddress(authorName, 1);
        initializeSignalStore(authorAddress, false);

        return decryptMessage(authorAddress, message);
    }

    private static UserKeyModel getUserKey(String username) throws EncryptionException {
        String requestUrl = Network.getClientRole() == Role.USER ?
                "/employees/" + username :
                "/users/" + username;

        UserKeyModel userKey;
        try {
            userKey = Network.makeGetRequest(requestUrl, UserKeyModel.class);
        } catch (RequestException e) {
            throw new EncryptionException(e.getMessage());
        }

        return userKey;
    }

    private static byte[] decryptMessage(SignalProtocolAddress sourceAddress, String message)
            throws EncryptionException {
        byte[] decodedEncryptedMessage = Base64.getDecoder().decode(message);
        SessionCipher clientSessionCipher = new SessionCipher(signalStore, sourceAddress);
        byte[] decryptedMessage;
        try {
            decryptedMessage = clientSessionCipher.decrypt(new SignalMessage(decodedEncryptedMessage));
        } catch (InvalidMessageException |
                 InvalidVersionException |
                 DuplicateMessageException |
                 NoSessionException |
                 UntrustedIdentityException |
                 InvalidKeyException |
                 LegacyMessageException e) {
            throw new EncryptionException(e.getMessage());
        }

        return decryptedMessage;
    }

    public static SignalProtocolAddress getAddress() {
        return new SignalProtocolAddress(Network.getClientName(), 1);
    }

    public static SignalProtocolAddress getSelfAddress() {
        return new SignalProtocolAddress(Network.getClientName(), 2);
    }

}
