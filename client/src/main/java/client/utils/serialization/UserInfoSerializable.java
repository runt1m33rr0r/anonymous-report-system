package client.utils.serialization;

import client.models.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoSerializable implements Serializable {

    @Serial
    private static final long serialVersionUID = 2368427920279981643L;

    private String clientName;
    private Role clientRole;

}
