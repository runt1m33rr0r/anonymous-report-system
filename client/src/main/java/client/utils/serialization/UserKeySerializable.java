package client.utils.serialization;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserKeySerializable implements Serializable {

    @Serial
    private static final long serialVersionUID = 8552205989854590058L;

    private byte[] clientIdentityPublicKey;
    private byte[] clientIdentityPrivateKey;
    private byte[] clientBasePublicKey;
    private byte[] clientBasePrivateKey;

}
