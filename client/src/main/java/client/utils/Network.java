package client.utils;

import client.exceptions.RequestException;
import client.models.ErrorModel;
import client.models.Role;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.springframework.http.MediaType;
import org.springframework.web.client.RestClient;

import java.text.MessageFormat;

public class Network {

    //public static final String SERVER_ADDRESS = "sbsbed4cw6ywkcqspxlvluduwhrg66xey4dmwayg6novdatkb2agmbyd.onion";
    public static final String SERVER_ADDRESS = "127.0.0.1";
    public static final String SERVER_NAME = "server";
    private static String TOKEN = null;
    private static String CLIENT_NAME = null;
    private static Role CLIENT_ROLE = null;

    public static void setToken(String token) {
        TOKEN = token;
    }

    public static void setClientName(String name) {
        CLIENT_NAME = name;
    }

    public static String getClientName() {
        return CLIENT_NAME;
    }

    public static void setClientRole(Role role) {
        CLIENT_ROLE = role;
    }

    public static Role getClientRole() {
        return CLIENT_ROLE;
    }

    public static <T> T makeGetRequest(String uri, Class<T> resultModelClass) throws RequestException {
        String jsonResponse = makeRequest(uri, null, RequestType.GET);

        return processJsonResponse(jsonResponse, resultModelClass);
    }

    public static <T> void makePutRequest(String uri, T requestData) throws RequestException {
        String jsonResponse = makeRequest(uri, requestData, RequestType.PUT);

        processError(jsonResponse);
    }

    public static <T> void makePostRequest(String uri, T requestData) throws RequestException {
        String jsonResponse = makeRequest(uri, requestData, RequestType.POST);

        processError(jsonResponse);
    }

    public static <T, E> T makePostRequest(String uri, E requestData, Class<T> resultModelClass)
            throws RequestException {
        String jsonResponse = makeRequest(uri, requestData, RequestType.POST);

        return processJsonResponse(jsonResponse, resultModelClass);
    }

    public static RestClient.RequestHeadersSpec<?> makeDownloadRequest(String uri) {
        RestClient client = RestClient.builder().build();
        String requestAddress = MessageFormat.format("http://{0}:8081/{1}", SERVER_ADDRESS, uri);
        String authHeaderName = "Authorization";
        String authHeader = MessageFormat.format("Bearer {0}", TOKEN);

        return client.get()
                .uri(requestAddress)
                .accept(MediaType.APPLICATION_OCTET_STREAM)
                .header(authHeaderName, authHeader);
    }

    private static <T> T processJsonResponse(String jsonResponse,
                                             Class<T> resultModelClass) throws RequestException {
        ObjectMapper mapper = JsonMapper
                .builder()
                .addModule(new JavaTimeModule())
                .build();
        try {
            ErrorModel error = mapper.readValue(jsonResponse, ErrorModel.class);
            throw new RequestException(error.getError());
        } catch (JsonProcessingException e) {
            try {
                return mapper.readValue(jsonResponse, resultModelClass);
            } catch (JsonProcessingException ex) {
                throw new RequestException(ex.getMessage());
            }
        }
    }

    private static void processError(String jsonResponse) throws RequestException {
        if (jsonResponse == null) {
            return;
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            ErrorModel error = mapper.readValue(jsonResponse, ErrorModel.class);

            throw new RequestException(error.getError());
        } catch (JsonProcessingException ex) {
            throw new RequestException(ex.getMessage());
        }
    }

    private static <E> String makeRequest(String uri, E requestData, RequestType requestType) {
//        HttpClient httpClient = HttpClient.create()
//                .proxy(proxy -> proxy
//                        .type(ProxyProvider.Proxy.SOCKS5)
//                        .host("127.0.0.1")
//                        .port(8080));
//        ReactorClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
//        final int size = 10 * 1024 * 1024;
//        final ExchangeStrategies strategies = ExchangeStrategies.builder()
//                .codecs(codecs -> codecs.defaultCodecs().maxInMemorySize(size))
//                .build();

//        SslContext sslContext;
//        try {
//            sslContext = SslContextBuilder
//                    .forClient()
//                    .trustManager(InsecureTrustManagerFactory.INSTANCE)
//                    .build();
//        } catch (SSLException e) {
//            throw new RequestException(e.getMessage());
//        }
//
//        HttpClient httpClient = HttpClient.create().secure(t -> t.sslContext(sslContext));
//        client = WebClient.builder()
//                .exchangeStrategies(strategies)
//                .clientConnector(new ReactorClientHttpConnector(httpClient))
//                .build();

        RestClient client = RestClient.builder().build();
        String requestAddress = MessageFormat.format("http://{0}:8081/{1}", SERVER_ADDRESS, uri);
        String authHeaderName = "Authorization";
        String authHeader = MessageFormat.format("Bearer {0}", TOKEN);

        if (requestType == RequestType.GET) {
            return client.get()
                    .uri(requestAddress)
                    .accept(MediaType.APPLICATION_JSON)
                    .header(authHeaderName, authHeader)
                    .retrieve()
                    .body(String.class);
        } else if (requestType == RequestType.PUT) {
            return client.put()
                    .uri(requestAddress)
                    .accept(MediaType.APPLICATION_JSON)
                    .header(authHeaderName, authHeader)
                    .body(requestData)
                    .retrieve()
                    .body(String.class);
        } else if (requestType == RequestType.POST) {
            return client.post()
                    .uri(requestAddress)
                    .accept(MediaType.APPLICATION_JSON)
                    .header(authHeaderName, authHeader)
                    .body(requestData)
                    .retrieve()
                    .body(String.class);
        } else {
            throw new RuntimeException("Invalid request type");
        }
    }

}
