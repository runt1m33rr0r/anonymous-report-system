package client.utils;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.function.Consumer;

public class UserInterface {

    private static Application application;
    private static Stage stage;

    public static void initialize(Application application, Stage stage) throws IOException {
        UserInterface.application = application;
        UserInterface.stage = stage;

        Parent root = loadScene("login.fxml");

        stage.setTitle("Report System");
        stage.setScene(new Scene(root));
        stage.show();
    }

    public static void pressButton(Button button, Runnable pressAction) {
        button.setDisable(true);

        pressAction.run();

        button.setDisable(false);
    }

    public static void showDialogMessage(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }

    public static Pair<Alert, ProgressBar> showProgressMessage(String title) {
        return showProgressMessage(title, null);
    }

    public static Pair<Alert, ProgressBar> showProgressMessage(String title, Runnable cancelAction) {
        Alert alert = new Alert(Alert.AlertType.NONE);
        alert.setTitle(title);
        alert.setHeaderText(null);

        ProgressBar progressBar = new ProgressBar(0);
        progressBar.setPrefSize(300, 30);
        progressBar.setProgress(0);

        Text progressText = new Text("0%");
        progressBar
                .progressProperty()
                .subscribe(value -> progressText.setText((int) (value.doubleValue() * 100) + "%"));

        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.getChildren().add(progressBar);
        vbox.getChildren().add(progressText);

        if (cancelAction != null) {
            Button cancelButton = new Button("Cancel");
            cancelButton.setOnAction(event -> cancelAction.run());
            vbox.getChildren().add(cancelButton);
        }

        alert.getDialogPane().setContent(vbox);
        alert.show();

        return new Pair<>(alert, progressBar);
    }

    public static void showMessageAsync(String title, String message) {
        Platform.runLater(() -> UserInterface.showDialogMessage(title, message));
    }

    public static void closeMessageAsync(Alert alertMessage) {
        Platform.runLater(() -> {
            alertMessage.getButtonTypes().add(ButtonType.CANCEL);
            alertMessage.hide();
            alertMessage.getButtonTypes().remove(ButtonType.CANCEL);
        });
    }

    public static void switchScene(String sceneName) {
        stage.getScene().setRoot(loadScene(sceneName));
    }

    public static void switchSceneContent(VBox container, String sceneName) {
        switchSceneContent(container, sceneName, null);
    }

    public static void switchSceneContent(VBox container, String sceneName, Object controller) {
        Node node = loadScene(sceneName, controller);
        if (container.getChildren().size() >= 2) {
            container.getChildren().remove(1);
        }

        container.getChildren().add(node);
    }

    public static void openSceneInNewWindow(String sceneName, String windowTitle) throws IOException {
        openSceneInNewWindow(sceneName, windowTitle, null);
    }

    public static void openSceneInNewWindow(
            String sceneName,
            String windowTitle,
            Consumer<Object> controllerCallback) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getResource(sceneName));
        Parent root = fxmlLoader.load();

        if (controllerCallback != null) {
            Object controller = fxmlLoader.getController();
            controllerCallback.accept(controller);
        }

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(root));
        stage.show();
    }

    private static <T> T loadScene(String sceneName) {
        return loadScene(sceneName, null);
    }

    private static <T> T loadScene(String sceneName, Object controller) {
        try {
            FXMLLoader loader = new FXMLLoader(getResource(sceneName));
            if (controller != null) {
                loader.setController(controller);
            }

            return loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static URL getResource(String resourceName) {
        URL resource = application.getClass().getClassLoader().getResource(resourceName);
        if (resource == null) {
            throw new RuntimeException(MessageFormat.format("Could not load resource {0}", resource));
        }

        return resource;
    }
}
