package client.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import client.services.UserService;
import client.utils.UserInterface;

public class PasswordController {

    @FXML
    private TextField password;

    @FXML
    private TextField repeatPassword;

    @FXML
    private Button changePassword;

    public void changePassword() {
        if (!password.getText().equals(repeatPassword.getText())) {
            UserInterface.showDialogMessage("Password change", "Passwords do not match!");

            return;
        }

        UserInterface.pressButton(changePassword, () -> {
            try {
                UserService.changePassword(password.getText());

                UserInterface.showDialogMessage("Password change", "Password changed!");
            } catch (Exception e) {
                UserInterface.showDialogMessage("Password change", e.getMessage());
            }
        });
    }
}
