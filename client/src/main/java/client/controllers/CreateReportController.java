package client.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;

import client.services.ReportService;
import client.utils.UserInterface;

public class CreateReportController {

    @FXML
    private TextField reportTitle;

    @FXML
    private TextArea reportDescription;

    @FXML
    private Button postReport;

    public void postReport() {
        UserInterface.pressButton(this.postReport, () -> {
            try {
                ReportService.createReport(this.reportTitle.getText(), this.reportDescription.getText());
                UserInterface.showDialogMessage("Create report process", "Report created!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                UserInterface.showDialogMessage("Create report process", e.getMessage());
            }
        });
    }
}
