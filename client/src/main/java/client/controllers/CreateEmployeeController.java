package client.controllers;

import client.exceptions.RequestException;
import client.services.UserService;
import client.utils.UserInterface;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;


public class CreateEmployeeController {

    @FXML
    private TextField employeeUsername;

    @FXML
    private TextField employeePassword;

    @FXML
    private TextField repeatEmployeePassword;

    @FXML
    private Button createEmployeeButton;

    public void createEmployee() {
        UserInterface.pressButton(this.createEmployeeButton, () -> {
            String registerPasswordText = this.employeePassword.getText();
            if (registerPasswordText.isBlank()) {
                UserInterface.showDialogMessage("Error", "Password can not be empty");
                return;
            }

            if (!registerPasswordText.equals(this.repeatEmployeePassword.getText())) {
                UserInterface.showDialogMessage("Error", "Passwords do not match");
                return;
            }

            try {
                UserService.createEmployee(this.employeeUsername.getText(), registerPasswordText);
            } catch (RequestException e) {
                UserInterface.showDialogMessage("Error", e.getMessage());
                return;
            }

            UserInterface.showDialogMessage("Employee creation", "Employee created");
        });
    }

}
