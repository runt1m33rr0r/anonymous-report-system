package client.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.util.Collection;

public class KeysController {

    @FXML
    private TextArea keys;

    public void setKeys(Collection<String> keysCollection) {
        StringBuilder keysBuilder = new StringBuilder();
        keysCollection.forEach(key -> keysBuilder.append(key).append("\n"));
        this.keys.setText(keysBuilder.toString());
    }
}
