package client.controllers;

import client.exceptions.RequestException;
import client.models.comments.CommentModel;
import client.models.reports.ReportStatus;
import client.models.reports.ReportListModel;
import client.services.ReportService;
import client.utils.Network;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import client.models.*;
import client.utils.UserInterface;
import lombok.Setter;
import org.signal.libsignal.protocol.InvalidKeyException;

import java.util.*;

public class ReportsTableController {

    @FXML
    private Label pageTitle;

    @FXML
    private TableView<ReportListModel.ReportModel> reportsTable;

    @FXML
    private TableColumn<ReportListModel.ReportModel, String> title;

    @FXML
    private TableColumn<ReportListModel.ReportModel, String> date;

    @FXML
    private ScrollPane reportsPane;

    @FXML
    private Pagination pagination;

    @Setter
    private ReportStatus reportState = ReportStatus.OPEN;

    @FXML
    public void initialize() {
        this.pagination.currentPageIndexProperty().addListener(
                (obs, oldIndex, newIndex) -> this.openPage(newIndex.intValue())
        );

        Role userRole = Network.getClientRole();
        if (userRole == Role.ADMIN) {
            return;
        }

        switch (this.reportState) {
            case OPEN -> this.pageTitle.setText("Open reports");
            case IN_PROGRESS -> this.pageTitle.setText("In progress reports");
            default -> this.pageTitle.setText("Closed reports");
        }

        this.title.setCellValueFactory(new PropertyValueFactory<>("title"));
        this.date.setCellValueFactory(new PropertyValueFactory<>("creationDate"));

        this.reportsTable.setRowFactory(tableView -> {
            TableRow<ReportListModel.ReportModel> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !row.isEmpty()) {
                    try {
                        List<CommentModel> comments = ReportService.getReportComments(row.getItem());
                        UserInterface.openSceneInNewWindow(
                                "report.fxml",
                                "Report",
                                controller -> {
                                    ReportController reportController = ((ReportController) controller);
                                    reportController.setReport(row.getItem());
                                    reportController.setComments(comments);
                                });
                    } catch (Exception e) {
                        UserInterface.showDialogMessage("Error", e.getMessage());
                    }
                }
            });

            return row;
        });

        this.openPage(0);
    }

    private void openPage(int page) {
        try {
            ReportListModel reports = ReportService.getReports(this.reportState, page);
            if (reports != null) {
                this.pagination.setPageCount(reports.getPagesCount());
                this.setReports(reports.getReports());
            }
        } catch (Exception e) {
            UserInterface.showDialogMessage("Error", e.getMessage());
        }
    }

    private void setReports(List<ReportListModel.ReportModel> reports) {
        Role userRole = Network.getClientRole();
        if (userRole == Role.ADMIN) {
            return;
        }

        this.reportsTable.getItems().clear();
        this.reportsTable.getItems().addAll(reports);
    }

}
