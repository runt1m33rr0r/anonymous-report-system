package client.controllers;

import client.models.Role;
import client.models.reports.ReportStatus;
import client.utils.Network;
import javafx.fxml.FXML;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import client.utils.UserInterface;
import javafx.scene.layout.VBox;

public class ProgramController {

    @FXML
    private VBox content;

    @FXML
    private MenuItem createReport;

    @FXML
    private MenuItem createEmployee;

    @FXML
    private MenuItem changePassword;

    @FXML
    private Menu myReportsMenu;

    @FXML
    public void initialize() {
        if (Network.getClientRole() == Role.USER || Network.getClientRole() == Role.EMPLOYEE) {
            this.createEmployee.setVisible(false);
        }

        if (Network.getClientRole() == Role.EMPLOYEE) {
            this.createEmployee.setVisible(false);
            this.createReport.setVisible(false);
        }

        if (Network.getClientRole() == Role.ADMIN) {
            this.myReportsMenu.setVisible(false);
        }
    }

    public void openCreateReport() {
        UserInterface.switchSceneContent(this.content, "create_report.fxml");
    }

    public void showOpenReports() {
        ReportsTableController controller = new ReportsTableController();
        controller.setReportState(ReportStatus.OPEN);
        UserInterface.switchSceneContent(this.content, "reports_list.fxml", controller);
    }

    public void showInProgressReports() {
        ReportsTableController controller = new ReportsTableController();
        controller.setReportState(ReportStatus.IN_PROGRESS);
        UserInterface.switchSceneContent(this.content, "reports_list.fxml", controller);
    }

    public void showClosedReports() {
        ReportsTableController controller = new ReportsTableController();
        controller.setReportState(ReportStatus.CLOSED);
        UserInterface.switchSceneContent(this.content, "reports_list.fxml", controller);
    }

    public void openChangePassword() {
        UserInterface.switchSceneContent(this.content, "change_password.fxml");
    }

    public void openCreateEmployee() {
        UserInterface.switchSceneContent(this.content, "create_employee.fxml");
    }

}
