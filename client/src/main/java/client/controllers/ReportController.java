package client.controllers;

import client.models.comments.CommentModel;
import client.models.reports.ReportListModel;
import client.models.reports.ReportStatus;
import client.services.ReportService;
import client.utils.Network;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import client.models.*;
import client.utils.UserInterface;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;
import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Consumer;

public class ReportController {

    @FXML
    private VBox commentsContainer;

    @FXML
    private TextArea comment;

    @FXML
    private Button refreshButton;

    @FXML
    private Button assignButton;

    @FXML
    private Button closeButton;

    @FXML
    private Button attachFileButton;

    @FXML
    private Tab closeTab;

    @FXML
    private Tab commentTab;

    @FXML
    private Label title;

    @FXML
    private Label date;

    @FXML
    private Label status;

    @FXML
    private Label attachedFileNameLabel;

    private ReportListModel.ReportModel report;

    private File selectedFile;

    public void setReport(ReportListModel.ReportModel report) {
        this.report = report;

        this.title.setText(this.report.getTitle());
        this.date.setText(this.report.getCreationDate().toString());
        this.status.setText(this.report.getStatus().toString());
        this.attachedFileNameLabel.setText("");

        if (Network.getClientRole() == Role.USER) {
            this.assignButton.setVisible(false);
        }

        this.updateReportStateButtons();
    }

    public void setComments(List<CommentModel> comments) {
        if (comments == null) {
            return;
        }

        this.commentsContainer.getChildren().clear();
        comments.forEach(this::addComment);
    }

    private void addComment(CommentModel comment) {
        String creationTime = comment.getCreationTime().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
        Label titleLabel = new Label(MessageFormat.format(
                "Posted on {0} by {1}",
                creationTime,
                comment.getAuthor()));
        TextArea content = new TextArea(comment.getContent());
        content.setEditable(false);
        content.setWrapText(true);
        content.setMouseTransparent(true);
        content.setFocusTraversable(false);
        content.autosize();
        titleLabel.setLabelFor(content);

        if (comment.getAttachedFileName() != null) {
            Button fileButton = new Button(MessageFormat.format(
                    "Download {0}",
                    comment.getAttachedFileName()));
            fileButton.setOnAction(event -> {
                final DirectoryChooser directoryChooser = new DirectoryChooser();
                File saveLocation = directoryChooser.showDialog(fileButton.getScene().getWindow());
                if (saveLocation == null) {
                    return;
                }

                ReportService.downloadFile(
                        this.report.getId(),
                        comment.getAuthor(),
                        comment.getAttachedFileName(),
                        saveLocation);
            });
            this.commentsContainer.getChildren().addAll(titleLabel, content, fileButton);
        } else {
            this.commentsContainer.getChildren().addAll(titleLabel, content);
        }
    }

    public void putReportInProgress() {
        this.changeReportState(ReportStatus.IN_PROGRESS);
    }

    public void closeReport() {
        this.changeReportState(ReportStatus.CLOSED);
    }

    public void attachFile() {
        final FileChooser fileChooser = new FileChooser();
        this.selectedFile = fileChooser.showOpenDialog(this.attachFileButton.getScene().getWindow());
        this.attachedFileNameLabel.setText(this.selectedFile.getName());
    }

    public void postComment() {
        Consumer<String> showCommentPostFailure = (String message) -> UserInterface.showDialogMessage(
                "Post comment failure",
                message);

        try {
            CommentModel comment;
            if (this.selectedFile == null) {
                comment = ReportService.createComment(
                        this.report.getId(),
                        this.comment.getText(),
                        this.report.getAuthor(),
                        this.report.getAgent());
                if (comment == null) {
                    showCommentPostFailure.accept("Comment is null");
                } else {
                    this.addComment(comment);
                }
            } else {
                ReportService.attachFile(
                        this.report.getId(),
                        this.comment.getText(),
                        this.report.getAuthor(),
                        this.report.getAgent(),
                        this.selectedFile,
                        this::addComment);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            showCommentPostFailure.accept(e.getMessage());
        }
    }

    private void changeReportState(ReportStatus newState) {
//        try {
//            UpdateReportStateModel reportUpdate = ReportService.changeReportState(
//                    this.report.getId(),
//                    newState);
//            this.report = new ReportModel(
//                    this.report.getId(),
//                    this.report.getTitle(),
//                    this.report.getDate(),
//                    reportUpdate.getNewState());
//            this.updateReportStateButtons();
//            this.status.setText(this.report.getState().toString());
//        } catch (Exception e) {
//            UserInterface.showDialogMessage(
//                    "Report state change failure",
//                    "Could not change report state!");
//        }
    }

    private void updateReportStateButtons() {
        if (this.report.getStatus() != ReportStatus.IN_PROGRESS) {
            this.assignButton.setVisible(false);
        }

        if (this.report.getStatus() == ReportStatus.CLOSED) {
            this.closeTab.setDisable(true);
            this.commentTab.setDisable(true);
        }
    }
}
