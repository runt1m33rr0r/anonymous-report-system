package client.controllers;

import client.exceptions.RequestException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import client.services.UserService;
import client.utils.UserInterface;

public class LoginController {

    @FXML
    private TextField loginUsername;

    @FXML
    private TextField loginPassword;

    @FXML
    private Button loginButton;

    @FXML
    private TextField registerUsername;

    @FXML
    private TextField registerPassword;

    @FXML
    private TextField registerRepeatPassword;

    @FXML
    private Button registerButton;

    public void initiateLogin() {
        UserInterface.pressButton(this.loginButton, () -> {
            try {
                String username = this.loginUsername.getText();
                if ("admin".equals(username)) {
                    UserService.loginAdmin(username, this.loginPassword.getText());
                } else {
                    UserService.loginUser(username, this.loginPassword.getText());
                }

                UserInterface.showDialogMessage("Log in process", "Successfully logged in!");
                UserInterface.switchScene("program.fxml");
            } catch (Exception e) {
                UserInterface.showDialogMessage("Login failed", e.getMessage());
            }
        });
    }

    public void initiateRegister() {
        UserInterface.pressButton(this.registerButton, () -> {
            String registerPasswordText = this.registerPassword.getText();
            if (registerPasswordText.isBlank()) {
                UserInterface.showDialogMessage("Error", "Password can not be empty");
                return;
            }

            if (!registerPasswordText.equals(this.registerRepeatPassword.getText())) {
                UserInterface.showDialogMessage("Error", "Passwords do not match");
                return;
            }

            try {
                UserService.createUser(this.registerUsername.getText(), registerPasswordText);
            } catch (RequestException e) {
                UserInterface.showDialogMessage("Error", e.getMessage());
                return;
            }

            UserInterface.showDialogMessage("Registration process", "Successfully registered!");
        });
    }

    private void showMessageAsync(String title, String message) {
        Platform.runLater(() -> UserInterface.showDialogMessage(title, message));
    }

    private void closeMessageAsync(Alert alertMessage) {
        Platform.runLater(() -> {
            alertMessage.getButtonTypes().add(ButtonType.CANCEL);
            alertMessage.hide();
            alertMessage.getButtonTypes().remove(ButtonType.CANCEL);
        });
    }

//    private void downloadFile() {
//        FileInfoModel fileInfo;
//        try {
//            fileInfo = SecureNetwork.makeGetRequest(
//                    "/files/download/picture.jpg",
//                    FileInfoModel.class);
//        } catch (EncryptionException | RequestException e) {
//            UserInterface.showDialogMessage("Error", "Could not get file information");
//
//            return;
//        }
//
//        Pair<Alert, ProgressBar> progress = UserInterface.showProgressMessage("progress");
//        Task<Void> task = new Task<>() {
//            @Override
//            public Void call() {
//                updateProgress(0, 1);
//
//                for (int index = 0; index < fileInfo.getChunksCount(); index++) {
//                    try (FileOutputStream fos = new FileOutputStream(
//                            "/home/user/Desktop/picture.jpg",
//                            true)) {
//                        ChunkModel chunk = SecureNetwork.makeGetRequest(
//                                "/files/download/picture.jpg/" + index,
//                                ChunkModel.class);
//
//                        fos.write(chunk.getData());
//                    } catch (EncryptionException | RequestException e) {
//                        closeMessageAsync(progress.getKey());
//                        showMessageAsync("Error", "Download failed");
//
//                        return null;
//                    } catch (IOException e) {
//                        closeMessageAsync(progress.getKey());
//                        showMessageAsync("Error", "Writing file error");
//
//                        return null;
//                    }
//
//                    updateProgress((index + 1) * 0.1, 1);
//                }
//
//                closeMessageAsync(progress.getKey());
//
//                return null;
//            }
//        };
//
//        progress.getValue().progressProperty().bind(task.progressProperty());
//        new Thread(task).start();
//    }

//    private void uploadFile(int chunkSize) {
//        String fileName = "picture.jpg";
//        Path inputFilePath = Paths.get("/media/user/f4db8f27-2a91-4015-a9bb-6d655c9cc330/" + fileName);
//        File inputFile = inputFilePath.toFile();
//        if (!inputFile.exists() || !inputFile.isFile()) {
//            UserInterface.showDialogMessage("Error", "File for upload does not exist");
//        }
//
//        long chunksCount = inputFile.length() / chunkSize;
//        long chunkedSize = chunksCount * chunkSize;
//        final int remainderChunkSize = (int) (inputFile.length() - chunkedSize);
//        if (remainderChunkSize > 0) {
//            chunksCount += 1;
//        }
//
//        Pair<Alert, ProgressBar> progress = UserInterface.showProgressMessage("progress");
//        long finalChunksCount = chunksCount;
//        Task<Void> task = new Task<>() {
//            @Override
//            protected Void call() {
//                updateProgress(0, 1);
//
//                for (int index = 0; index < finalChunksCount; index++) {
//                    try (RandomAccessFile raf = new RandomAccessFile(inputFile, "r")) {
//                        FileChannel channel = raf.getChannel();
//                        ByteBuffer buffer;
//                        boolean isLast = index >= finalChunksCount - 1;
//                        if (remainderChunkSize > 0 && isLast) {
//                            channel.position(chunkedSize);
//                            buffer = ByteBuffer.allocate(remainderChunkSize);
//                        } else {
//                            channel.position((long) index * chunkSize);
//                            buffer = ByteBuffer.allocate(chunkSize);
//                        }
//
//                        channel.read(buffer);
//
//                        SecureNetwork.makePostRequest(
//                                "/files",
//                                new ChunkModel(fileName, buffer.array(), index == 0, isLast),
//                                ChunkModel.class);
//                    } catch (IOException | EncryptionException e) {
//                        closeMessageAsync(progress.getKey());
//                        showMessageAsync("File error", "Upload failed");
//
//                        return null;
//                    } catch (RequestException e) {
//                        closeMessageAsync(progress.getKey());
//                        showMessageAsync("Request error", e.getMessage());
//
//                        return null;
//                    }
//
//                    updateProgress((index + 1) * 0.1, 1);
//                }
//
//                closeMessageAsync(progress.getKey());
//
//                return null;
//            }
//        };
//
//        progress.getValue().progressProperty().bind(task.progressProperty());
//        new Thread(task).start();
//    }

}
