package client.exceptions;

public class RequestException extends Exception {

    public RequestException(String message) {
        super(message);
    }
}
