package com.file.server.controllers;

import com.file.server.exceptions.ValidationException;
import com.file.server.models.CommentModel;
import com.file.server.models.CreateCommentModel;
import com.file.server.models.ReportModel;
import com.file.server.services.implementations.PrimaryServerClient;
import com.file.server.services.FileService;
import feign.FeignException;
import lombok.AllArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@AllArgsConstructor
@RestController
@RequestMapping("/files")
public class FileController {

    private final FileService fileService;
    private final PrimaryServerClient centralServerClient;

    @PostMapping
    public ResponseEntity<CommentModel> uploadFile(@RequestParam("files") MultipartFile[] files,
                                                   @RequestPart("comment") CreateCommentModel comment,
                                                   @RequestHeader("user") String username,
                                                   @RequestHeader("userId") String userId,
                                                   @RequestHeader("role") String role) {
        if (files.length != 2) {
            throw new ValidationException("Incorrect number of files to upload");
        }

        Long reportId = comment.getReportId();
        ReportModel report = this.centralServerClient.getReport(username, role, reportId);
        Long loggedInUserId = Long.valueOf(userId);
        Long recipientId;
        if (loggedInUserId.equals(report.getAuthorId())) {
            recipientId = report.getAgentId();
        } else if (loggedInUserId.equals(report.getAgentId())) {
            recipientId = report.getAuthorId();
        } else {
            throw new ValidationException("Requesting user is not part of report");
        }

        try {
            this.fileService.uploadFile(
                    files[0],
                    loggedInUserId,
                    report.getId());
            this.fileService.uploadFile(
                    files[1],
                    recipientId,
                    report.getId()
            );
        } catch (IOException e) {
            throw new ValidationException("Error while saving file on storage");
        }

        comment.setAttachedFileName(files[0].getOriginalFilename());
        CommentModel commentModel;
        try {
            commentModel = this.centralServerClient.createComment(username, role, comment);
        } catch (FeignException e) {
            this.fileService.deleteFile(files[0].getOriginalFilename(), loggedInUserId, reportId);
            this.fileService.deleteFile(files[1].getOriginalFilename(), recipientId, reportId);

            throw e;
        }

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(commentModel);
    }

    @GetMapping("/{report_id}/{file_name}")
    public ResponseEntity<InputStreamResource> downloadFile(@PathVariable("report_id") Long reportId,
                                                           @PathVariable("file_name") String fileName,
                                                           @RequestHeader("userId") String userId) throws FileNotFoundException {
        Long loggedInUserId = Long.valueOf(userId);
        File file = this.fileService.getFile(loggedInUserId, reportId, fileName);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(file.length())
                .body(resource);
    }

}
