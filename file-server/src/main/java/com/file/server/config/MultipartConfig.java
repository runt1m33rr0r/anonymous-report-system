package com.file.server.config;

import jakarta.servlet.MultipartConfigElement;
import lombok.AllArgsConstructor;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.util.unit.DataSize;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@AllArgsConstructor
public class MultipartConfig implements WebMvcConfigurer {

    private final Environment env;

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(DataSize.ofGigabytes(100));
        factory.setMaxRequestSize(DataSize.ofGigabytes(100));
        factory.setFileSizeThreshold(DataSize.ofMegabytes(5));
        factory.setLocation(this.env.getProperty("temp.location"));

        return factory.createMultipartConfig();
    }

}