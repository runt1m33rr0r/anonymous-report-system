package com.file.server.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentModel {
    private long id;
    private String author;
    private String content;
    private LocalDateTime creationTime;
    private String attachedFileName;
}