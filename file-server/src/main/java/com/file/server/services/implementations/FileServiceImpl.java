package com.file.server.services.implementations;

import com.file.server.exceptions.StorageException;
import com.file.server.exceptions.ValidationException;
import com.file.server.services.FileService;
import lombok.AllArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@AllArgsConstructor
public class FileServiceImpl implements FileService {

    private final Environment env;

    @Override
    public File getFile(Long recipientId, Long reportId, String fileName) {
        File file = Paths.get(
                this.getFileStorage(),
                String.valueOf(recipientId),
                String.valueOf(reportId),
                fileName).toFile();
        if (!file.exists()) {
            throw new StorageException("File %s does not exist".formatted(file.getName()));
        }

        return file;
    }

    @Override
    public void uploadFile(MultipartFile file, Long recipientId, Long reportId) {
        Path destinationPath = Paths.get(
                this.getFileStorage(),
                String.valueOf(recipientId),
                String.valueOf(reportId));
        if (Files.notExists(destinationPath)) {
            try {
                Files.createDirectories(destinationPath);
            } catch (IOException e) {
                throw new StorageException("Could not create file save destination");
            }
        }

        File destinationFile = Paths.get(destinationPath.toString(), file.getOriginalFilename()).toFile();
        if (destinationFile.exists()) {
            throw new ValidationException("File with this name has already been uploaded");
        }

        try {
            file.transferTo(destinationFile);
        } catch (IOException e) {
            throw new StorageException("Could not transfer uploaded temporary %s file to destination %s".formatted(
                    file.getName(),
                    destinationFile.getName()));
        }
    }

    @Override
    public void deleteFile(String fileName, Long recipientId, Long reportId) {
        File destinationPath = Paths.get(
                this.getFileStorage(),
                String.valueOf(recipientId),
                String.valueOf(reportId),
                fileName).toFile();
        if (!destinationPath.exists()) {
            throw new StorageException("File %s does not exist".formatted(destinationPath.getPath()));
        }

        if (!destinationPath.delete()) {
            throw new StorageException("Could not delete file upload: " + destinationPath.getPath());
        }
    }

    public String getFileStorage() {
        String filesPathName = this.env.getProperty("files.location");
        if (filesPathName == null) {
            throw new StorageException("Could not read file storage location config");
        }

        if (!filesPathName.endsWith("/")) {
            filesPathName = filesPathName + "/";
        }

        return filesPathName;
    }

}
