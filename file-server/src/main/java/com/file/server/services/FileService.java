package com.file.server.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface FileService {

    void uploadFile(MultipartFile file, Long recipientId, Long reportId) throws IOException;

    File getFile(Long recipientId, Long reportId, String fileName);

    void deleteFile(String fileName, Long recipientId, Long reportId);

}
