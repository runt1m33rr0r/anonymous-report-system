package com.file.server.services.implementations;

import com.file.server.models.CommentModel;
import com.file.server.models.CreateCommentModel;
import com.file.server.models.ReportModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "primary-server")
public interface PrimaryServerClient {

    @GetMapping("/reports/{reportId}")
    ReportModel getReport(@RequestHeader("user") String username,
                          @RequestHeader("role") String role,
                          @PathVariable("reportId") Long reportId);

    @PostMapping("/comments")
    CommentModel createComment(@RequestHeader("user") String username,
                               @RequestHeader("role") String role,
                               CreateCommentModel createCommentModel);

}
