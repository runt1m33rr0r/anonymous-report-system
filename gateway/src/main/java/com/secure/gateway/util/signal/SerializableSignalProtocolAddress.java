package com.secure.gateway.util.signal;

import org.signal.libsignal.protocol.ServiceId;
import org.signal.libsignal.protocol.SignalProtocolAddress;

import java.io.Serializable;

public class SerializableSignalProtocolAddress extends SignalProtocolAddress implements Serializable {

    public SerializableSignalProtocolAddress(String name, int deviceId) {
        super(name, deviceId);
    }

    public SerializableSignalProtocolAddress(ServiceId serviceId, int deviceId) {
        super(serviceId, deviceId);
    }

    public SerializableSignalProtocolAddress(long unsafeHandle) {
        super(unsafeHandle);
    }

    public SerializableSignalProtocolAddress(SignalProtocolAddress protocolAddress) {
        super(protocolAddress.getName(), protocolAddress.getDeviceId());
    }

}
