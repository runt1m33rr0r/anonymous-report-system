package com.secure.gateway.util.signal;

import org.signal.libsignal.protocol.groups.state.InMemorySenderKeyStore;

import java.io.Serializable;

public class SerializableSenderKeyStore extends InMemorySenderKeyStore implements Serializable {
}
