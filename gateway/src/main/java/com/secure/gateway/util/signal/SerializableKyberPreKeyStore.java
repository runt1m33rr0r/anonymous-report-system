package com.secure.gateway.util.signal;

import org.signal.libsignal.protocol.state.impl.InMemoryKyberPreKeyStore;

import java.io.Serializable;

public class SerializableKyberPreKeyStore extends InMemoryKyberPreKeyStore implements Serializable {
}
