package com.secure.gateway.util;

import java.util.Iterator;

public class FileReader implements Iterable<byte[]> {



    @Override
    public Iterator<byte[]> iterator() {
        return new Iterator<>() {


            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public byte[] next() {
                return new byte[0];
            }
        };
    }
}
