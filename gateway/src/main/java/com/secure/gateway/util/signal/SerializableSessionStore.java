package com.secure.gateway.util.signal;

import org.signal.libsignal.protocol.state.impl.InMemorySessionStore;

import java.io.Serializable;

public class SerializableSessionStore extends InMemorySessionStore implements Serializable {
}
