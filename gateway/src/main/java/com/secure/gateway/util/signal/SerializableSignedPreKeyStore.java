package com.secure.gateway.util.signal;

import org.signal.libsignal.protocol.state.impl.InMemorySignedPreKeyStore;

import java.io.Serializable;

public class SerializableSignedPreKeyStore extends InMemorySignedPreKeyStore implements Serializable {
}
