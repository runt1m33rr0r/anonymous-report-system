package com.secure.gateway;

import com.secure.gateway.models.BundleModel;
import com.secure.gateway.models.SignalProtocolStoreParameters;
import com.secure.gateway.services.SignalService;
import lombok.AllArgsConstructor;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@AllArgsConstructor
public class GatewayApplication {

	private final SignalService signalService;

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	@Bean
	public NewTopic loginTopic() {
		return TopicBuilder.name("login").build();
	}

	@Bean
	public NewTopic signalStoreParamsTopic() {
		return TopicBuilder.name("signalStoreParams").build();
	}

	@KafkaListener(groupId = "bundles-#{environment['server.port']}", topics = "login")
	public void listenClientBundle(BundleModel in) {
		System.out.println(in.username());
	}

	@KafkaListener(groupId = "stores-#{environment['server.port']}", topics = "signalStoreParams")
	public void listenSignalStoreParameters(SignalProtocolStoreParameters in) {
		signalService.initialize(in);

		System.out.println("Signal protocol store initialized");
	}

}
