package com.secure.gateway.models;

public record ErrorModel(String error) { }
