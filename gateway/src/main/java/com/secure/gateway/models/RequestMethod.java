package com.secure.gateway.models;

public enum RequestMethod {
    GET,
    POST,
    PUT,
    DELETE
}