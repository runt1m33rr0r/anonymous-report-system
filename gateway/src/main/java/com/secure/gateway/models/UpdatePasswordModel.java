package com.secure.gateway.models;

public record UpdatePasswordModel(String password) { }