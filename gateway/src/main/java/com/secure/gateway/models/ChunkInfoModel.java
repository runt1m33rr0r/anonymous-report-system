package com.secure.gateway.models;

public record ChunkInfoModel(int chunkSize, long maxChunkCount) { }