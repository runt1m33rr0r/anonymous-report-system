package com.secure.gateway.models;

public record ChunkModel(String fileName, byte[] data, boolean isFirst, boolean isLast) { }