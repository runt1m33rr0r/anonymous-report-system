package com.secure.gateway.models;

public record EncryptedDataModel(byte[] data) { }