package com.secure.gateway.models;

public record FileInfoModel(String name, long chunksCount) { }