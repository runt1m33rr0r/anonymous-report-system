package com.secure.gateway.models;

public record BundleModel(String username,
                          byte[] basePublicKey,
                          byte[] identityPublicKey) { }