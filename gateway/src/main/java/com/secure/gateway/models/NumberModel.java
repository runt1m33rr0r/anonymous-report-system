package com.secure.gateway.models;

public record NumberModel(Integer value) { }