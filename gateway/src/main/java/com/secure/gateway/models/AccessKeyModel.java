package com.secure.gateway.models;

public record AccessKeyModel(String value) { }