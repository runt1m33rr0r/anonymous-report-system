package com.secure.gateway.models;

public record DecryptedDataModel(String path, RequestMethod requestMethod, String data) { }