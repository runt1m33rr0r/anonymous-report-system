package com.secure.gateway.config;

import com.secure.gateway.services.FileService;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class ScheduledTasks {

    private final FileService fileService;

    @Scheduled(fixedRate = 30, timeUnit = TimeUnit.MINUTES)
    public void cleanFiles() {
        int maxFileAgeMinutes = this.fileService.getTempFileMaxAgeMinutes();
        System.out.println(MessageFormat.format(
                "cleaning temp files that have not been modified in {0} minutes",
                maxFileAgeMinutes));

        String fileStorage = this.fileService.getFileStorage();
        try (Stream<Path> walk = Files.walk(Path.of(fileStorage))) {
            walk.forEach(path -> {
                File file = path.toFile();
                boolean fileExists = file.exists() && file.isFile();
                boolean isTempFile = file.getName().endsWith(this.fileService.getTempFileSuffix());
                if (fileExists && isTempFile) {
                    try {
                        FileTime fileTime = Files.getLastModifiedTime(path);
                        LocalDateTime lastModified = LocalDateTime.ofInstant(fileTime.toInstant(), ZoneId.systemDefault());
                        LocalDateTime modifiedMaximumTime = LocalDateTime.now().minusMinutes(maxFileAgeMinutes);
                        if (lastModified.isBefore(modifiedMaximumTime)) {
                            if (file.delete()) {
                                System.out.println(MessageFormat.format("Deleted file {0}", file.toString()));
                            } else {
                                throw new RuntimeException(MessageFormat.format(
                                        "Could not delete file {0}",
                                        file.toString()));
                            }
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(MessageFormat.format(
                                "Error while reading file last modified time {0}",
                                file.toString()));
                    }
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
