package com.secure.gateway.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.secure.gateway.models.DecryptedDataModel;
import com.secure.gateway.models.EncryptedDataModel;
import com.secure.gateway.models.RequestMethod;
import com.secure.gateway.services.SignalService;
import lombok.AllArgsConstructor;
import org.signal.libsignal.protocol.UntrustedIdentityException;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.rewrite.ModifyRequestBodyGatewayFilterFactory;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Configuration
@AllArgsConstructor
public class Routes {

    private final SignalService signalService;
    private final ModifyRequestBodyGatewayFilterFactory requestBodyFactory;
    private final Environment environment;

    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder) {
        String port = this.environment.getProperty("server.port");
        String address = this.environment.getProperty("server.address");

        return builder.routes()
                .route(p -> p
                        .path("/api")
                        .and().method(HttpMethod.POST)
                        .and().readBody(String.class, requestBody -> true)
                        .filters(f -> f
                                .setRequestHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                .setResponseHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                .filter(this::modifyRequest)
                                .modifyResponseBody(String.class, String.class, this::modifyResponseBody)
                                .setStatus(HttpStatus.OK))
                        .uri(String.format("http://%s:%s", address, port)))
                .build();
    }

    private Mono<Void> modifyRequest(ServerWebExchange exchange, GatewayFilterChain chain) {
        String authKey = this.getAuthHeader(exchange.getRequest().getHeaders());
        if (authKey.isBlank()) {
            return chain.filter(exchange.mutate().request(exchange
                            .getRequest()
                            .mutate()
                            .path("/users/login")
                            .build())
                    .build());
        }

        String body = exchange.getAttribute("cachedRequestBodyObject");
        ObjectMapper mapper = new ObjectMapper();
        try {
            EncryptedDataModel encryptedData = mapper.readValue(body, EncryptedDataModel.class);
            byte[] decryptedBytes = signalService.decryptMessage(authKey, encryptedData.data());
            String decryptedText = new String(decryptedBytes, StandardCharsets.UTF_8);
            DecryptedDataModel decryptedData = mapper.readValue(decryptedText, DecryptedDataModel.class);

            return this.modifyRequestBody(exchange, chain, decryptedData);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());

            return chain.filter(exchange);
        }
    }

    private Mono<Void> modifyRequestBody(ServerWebExchange exchange,
                                          GatewayFilterChain chain,
                                          DecryptedDataModel decryptedData) {
        GatewayFilterChain modifiedChain = ch -> chain.filter(ch.mutate().request(ch
                        .getRequest()
                        .mutate()
                        .path(decryptedData.path())
                        .method(HttpMethod.valueOf(decryptedData.requestMethod().name()))
                        .build())
                .build());

        // GET and DELETE requests do not have a body
        if (decryptedData.requestMethod() == RequestMethod.GET ||
                decryptedData.requestMethod() == RequestMethod.DELETE) {
            return modifiedChain.filter(exchange);
        }

        ModifyRequestBodyGatewayFilterFactory.Config reqCfg = new ModifyRequestBodyGatewayFilterFactory.Config();
        reqCfg.setRewriteFunction(String.class, String.class, (exc, b) -> Mono.just(decryptedData.data()));
        GatewayFilter modifyReqBodyFilter = requestBodyFactory.apply(reqCfg);

        return modifyReqBodyFilter.filter(exchange, modifiedChain);
    }

    private Mono<String> modifyResponseBody(ServerWebExchange exchange, String body) {
        if (body == null) {
            return Mono.just("");
        }

        String authKey = this.getAuthHeader(exchange.getRequest().getHeaders());
        if (authKey.isBlank()) {
            return Mono.just(body);
        }

        try {
            byte[] encryptedData = signalService.encryptMessage(authKey, body);
            ObjectMapper mapper = new ObjectMapper();
            String encryptedRes = mapper.writeValueAsString(new EncryptedDataModel(encryptedData));

            return Mono.just(encryptedRes);
        } catch (UntrustedIdentityException | JsonProcessingException e) {
            System.out.println(e.getMessage());;
        }

        return Mono.just(body);
    }

    private String getAuthHeader(HttpHeaders inputHeaders) {
        String authHeaderName = "auth";
        try {
            if (!inputHeaders.containsKey(authHeaderName) ||
                    inputHeaders.get(authHeaderName) == null ||
                    Objects.requireNonNull(inputHeaders.get(authHeaderName)).isEmpty()) {
                return "";
            }

            return Objects.requireNonNull(inputHeaders.get(authHeaderName)).get(0);
        } catch (NullPointerException ex) {
            return "";
        }
    }
}
