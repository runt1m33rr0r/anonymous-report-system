package com.secure.gateway.controllers;

import com.secure.gateway.models.ChunkInfoModel;
import com.secure.gateway.models.ChunkModel;
import com.secure.gateway.models.FileInfoModel;
import com.secure.gateway.services.FileService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.text.MessageFormat;

@AllArgsConstructor
@RestController
@RequestMapping("/files")
public class FileController {

    private static final int chunkSize = 307200;

    private final FileService fileService;

    @GetMapping("/info")
    public ChunkInfoModel getChunkInfo() {
        // chunk size - 10 KB, max file size - 1 GB

        return new ChunkInfoModel(chunkSize, chunkSize);
    }

    @GetMapping("/download/{file_name}")
    public FileInfoModel getFileInfo(@PathVariable("file_name") String fileName) {
        File inputFile = this.getFile(fileName);
        checkFileExists(inputFile);

        long chunksCount = inputFile.length() / chunkSize;
        long chunkedSize = chunksCount * chunkSize;
        long remainderChunkSize = inputFile.length() - chunkedSize;

        if (remainderChunkSize > 0) {
            chunksCount += 1;
        }

        return new FileInfoModel(fileName, chunksCount);
    }

    @PostMapping()
    public ChunkModel uploadChunk(@RequestBody ChunkModel chunk) {
        String fileStorage = this.fileService.getFileStorage();
        String partiallyUploadedFileName = fileStorage + chunk.fileName() + this.fileService.getTempFileSuffix();
        File partiallyUploadedFile = Paths.get(partiallyUploadedFileName).toFile();
        if (!chunk.isFirst()) {
            checkFileExists(partiallyUploadedFile);
        } else if (partiallyUploadedFile.exists() && !partiallyUploadedFile.delete()) {
            throw new RuntimeException("Could not delete partial upload of previous attempt for this file");
        }

        try (FileOutputStream output = new FileOutputStream(partiallyUploadedFile.getAbsolutePath(), true)) {
            output.write(chunk.data());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (chunk.isLast()) {
            String fullyUploadedFileName = fileStorage + chunk.fileName();
            boolean success = partiallyUploadedFile.renameTo(new File(fullyUploadedFileName));
            if (!success) {
                throw new RuntimeException(MessageFormat.format(
                        "Could not save file {0}",
                        fullyUploadedFileName));
            }
        }

        return chunk;
    }

    @GetMapping("/download/{file_name}/{index}")
    public ChunkModel downloadChunk(@PathVariable("file_name") String fileName, @PathVariable("index") long index) {
        File inputFile = this.getFile(fileName);
        checkFileExists(inputFile);

        long chunksCount = inputFile.length() / chunkSize;
        long chunkedSize = chunksCount * chunkSize;
        int remainderChunk = (int) (inputFile.length() - chunkedSize);
        boolean isLast = index >= chunksCount;

        try (RandomAccessFile raf = new RandomAccessFile(inputFile, "r")) {
            FileChannel channel = raf.getChannel();
            ByteBuffer buffer;
            if (isLast) {
                channel.position(chunkedSize);
                buffer = ByteBuffer.allocate(remainderChunk);
            } else {
                channel.position(index * chunkSize);
                buffer = ByteBuffer.allocate(chunkSize);
            }

            channel.read(buffer);

            return new ChunkModel(fileName, buffer.array(), index == 0, isLast);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private File getFile(String fileName) {
        return Paths.get(this.fileService.getFileStorage() + fileName).toFile();
    }

    private static void checkFileExists(File file) {
        if (!file.exists() || !file.isFile()) {
            throw new RuntimeException(MessageFormat.format("File {0} does not exist", file));
        }
    }

    private static void appendToFile(FileChannel fileChannel, int bufferSize) {
        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
        try (FileOutputStream fos = new FileOutputStream("/home/user/Desktop/picture.jpg", true)) {
            fileChannel.read(buffer);
            fos.write(buffer.array());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
