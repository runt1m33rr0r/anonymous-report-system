package com.secure.gateway.controllers;

import com.secure.gateway.models.AccessKeyModel;
import com.secure.gateway.models.UpdatePasswordModel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class AuthController {

    @PutMapping("/password")
    public UpdatePasswordModel updatePassword(@RequestBody @Valid UpdatePasswordModel updatePasswordModel) {
        System.out.println("new password: " + updatePasswordModel.password());

        return updatePasswordModel;
    }

    @GetMapping("/employee")
    public AccessKeyModel createEmployee() {
        return new AccessKeyModel("access key");
    }
}
