package com.secure.gateway.services;

import java.nio.file.Path;

public interface FileService {

    String getFileStorage();

    String getTempFileSuffix();

    int getTempFileMaxAgeMinutes();
}