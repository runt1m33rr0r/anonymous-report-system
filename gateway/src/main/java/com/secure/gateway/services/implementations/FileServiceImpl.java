package com.secure.gateway.services.implementations;

import com.secure.gateway.services.FileService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
@AllArgsConstructor
public class FileServiceImpl implements FileService {

    private final Environment env;

    @Override
    public String getFileStorage() {
        String filesPathName = this.env.getProperty("files.location");
        if (filesPathName == null) {
            throw new RuntimeException("Could not read file storage location config");
        }

        if (!filesPathName.endsWith("/")) {
            filesPathName = filesPathName + "/";
        }

        return filesPathName;
    }

    @Override
    public String getTempFileSuffix() {
        String tempFileSuffix = this.env.getProperty("files.temp-suffix");
        if (tempFileSuffix == null) {
            throw new RuntimeException("Could not read temp files suffix config");
        }

        return tempFileSuffix;
    }

    @Override
    public int getTempFileMaxAgeMinutes() {
        String tempFileMaxAgeMinutes = this.env.getProperty("files.temp-max-age-minutes");
        if (tempFileMaxAgeMinutes == null) {
            throw new RuntimeException("Could not read temp files max age in minutes config");
        }

        return Integer.parseInt(tempFileMaxAgeMinutes);
    }
}
