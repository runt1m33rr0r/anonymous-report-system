package com.secure.gateway.services.implementations;

import com.secure.gateway.models.BundleModel;
import com.secure.gateway.models.SignalProtocolStoreParameters;
import com.secure.gateway.services.SignalService;
import com.secure.gateway.util.signal.SerializableSignalStore;
import lombok.AllArgsConstructor;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.protocol.*;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPrivateKey;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.message.CiphertextMessage;
import org.signal.libsignal.protocol.message.SignalMessage;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
@AllArgsConstructor
public class SignalServiceImpl implements SignalService {

    private static SerializableSignalStore serverStore;
    private static IdentityKeyPair serverIdentityKey;
    private static ECKeyPair serverBaseKey;

    @Override
    public void saveClientPreKeyBundle(BundleModel bundle) throws Exception {
        this.checkEncryptionInitialization();

        SignalProtocolAddress clientAddress = new SignalProtocolAddress(bundle.username(), 1);
        if (serverStore.containsSession(clientAddress)) {
            serverStore.deleteSession(clientAddress);
            //serverStore.saveIdentity(clientAddress, new IdentityKey(bundle.getIdentityPublicKey(), 0));
        }

        ECPublicKey clientBasePublicKey = ECPublicKey.fromPublicKeyBytes(bundle.basePublicKey());
        SessionRecord clientSessionRecord = SessionRecord.initializeBobSession(
                serverIdentityKey,
                serverBaseKey,
                serverBaseKey,
                new IdentityKey(bundle.identityPublicKey(), 0),
                clientBasePublicKey
        );

        serverStore.storeSession(clientAddress, clientSessionRecord);

    }

    @Override
    public byte[] encryptMessage(String destinationName, String sourceMessage) throws UntrustedIdentityException {
        this.checkEncryptionInitialization();

        SessionCipher serverCipher = new SessionCipher(
                serverStore,
                new SignalProtocolAddress(destinationName, 1));
        CiphertextMessage encryptedMessage = serverCipher.encrypt(sourceMessage.getBytes());

        return encryptedMessage.serialize();
    }

    @Override
    public byte[] decryptMessage(String sourceName, byte[] sourceMessage) {
        this.checkEncryptionInitialization();

        SessionCipher serverCipher = new SessionCipher(
                serverStore,
                new SignalProtocolAddress(sourceName, 1));
        byte[] decryptedClientMessage;
        try {
            decryptedClientMessage = serverCipher.decrypt(new SignalMessage(sourceMessage));
        } catch (NoSessionException | InvalidMessageException | UntrustedIdentityException | DuplicateMessageException |
                 InvalidVersionException | LegacyMessageException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }

        return decryptedClientMessage;
    }

    @Override
    public void initialize(SignalProtocolStoreParameters params) {
        byte[] serverBasePrivateKeySerialized = params.getServerBasePrivateKey();
        byte[] serverBasePublicKeySerialized = params.getServerBasePublicKey();
        serverBaseKey = new ECKeyPair(
                ECPublicKey.fromPublicKeyBytes(serverBasePublicKeySerialized),
                new ECPrivateKey(Native.ECPrivateKey_Deserialize(serverBasePrivateKeySerialized))
        );

        byte[] serializedServerIdentityKey = params.getServerIdentityKey();
        serverIdentityKey = new IdentityKeyPair(serializedServerIdentityKey);

        int serverRegistrationId = params.getServerRegistrationId();
        serverStore = new SerializableSignalStore(serverIdentityKey, serverRegistrationId);
    }

    private void checkEncryptionInitialization() {
        if (serverStore == null || serverIdentityKey == null || serverBaseKey == null) {
            throw new RuntimeException("Server encryption not initialized");
        }
    }
}
