package com.secure.gateway.services;

import com.secure.gateway.models.BundleModel;
import com.secure.gateway.models.SignalProtocolStoreParameters;
import org.signal.libsignal.protocol.UntrustedIdentityException;

public interface SignalService {

    void saveClientPreKeyBundle(BundleModel bundle) throws Exception;

    byte[] encryptMessage(String sourceName, String sourceMessage) throws UntrustedIdentityException;

    byte[] decryptMessage(String sourceName, byte[] sourceMessage);

    void initialize(SignalProtocolStoreParameters params);
}
