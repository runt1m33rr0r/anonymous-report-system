package com.gateway.entry.services;

import com.gateway.entry.models.Role;

public interface JwtService {

    boolean hasRole(String token, Role role);

    String extractUsername(String token);

    String extractUserId(String token);

}
