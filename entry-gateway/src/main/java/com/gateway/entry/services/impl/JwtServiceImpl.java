package com.gateway.entry.services.impl;

import com.gateway.entry.models.Role;
import com.gateway.entry.services.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.function.Function;

@Service
@AllArgsConstructor
public class JwtServiceImpl implements JwtService {

    private static final String KEY_STRING = """
				secret
				secret
				secret
				secret
				secret
				secret
				secret
				""";
    private static final SecretKey SECRET = Keys.hmacShaKeyFor(KEY_STRING.getBytes());

    @Override
    public boolean hasRole(String token, Role role) {
        if (!this.validateToken(token)) {
            return false;
        }

        String roleClaim = (String) this.extractClaim(token, claims -> claims.get("role"));
        return role.toString().equals(roleClaim);
    }

    @Override
    public String extractUsername(String token) {
        return this.extractClaim(token, Claims::getSubject);
    }

    @Override
    public String extractUserId(String token) {
        return (String) this.extractClaim(token, claims -> claims.get("id"));
    }

    private boolean validateToken(String token) {
        boolean tokenExpired = this.extractClaim(token, Claims::getExpiration).before(new Date());

        return !tokenExpired;
    }

    private <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = Jwts
                .parser()
                .verifyWith(SECRET)
                .build()
                .parseSignedClaims(token)
                .getPayload();
        return claimsResolver.apply(claims);
    }

}