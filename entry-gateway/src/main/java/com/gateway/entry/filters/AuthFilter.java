package com.gateway.entry.filters;

import com.gateway.entry.models.Role;
import com.gateway.entry.services.JwtService;
import lombok.Data;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthFilter extends AbstractGatewayFilterFactory<AuthFilter.Config> {

    private final JwtService jwtService;

    public AuthFilter(JwtService jwtService) {
        super(Config.class);

        this.jwtService = jwtService;
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            ServerHttpResponse response = exchange.getResponse();
            List<String> authHeaders = request.getHeaders().get("Authorization");

            if (authHeaders == null || authHeaders.isEmpty()) {
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }

            String token = authHeaders.getFirst().substring("Bearer ".length());
            for (String role : config.getRoles().split("_")) {
                Role userRole = Role.valueOf(role);
                if (this.jwtService.hasRole(token, userRole)) {
                    return chain.filter(exchange.mutate().request(req ->
                            req.headers(httpHeaders -> {
                                httpHeaders.add("user", this.jwtService.extractUsername(token));
                                httpHeaders.add("userId", this.jwtService.extractUserId(token));
                                httpHeaders.add("role", role);
                            }))
                            .build());
                }
            }

            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        };
    }

    @Data
    public static class Config {
        private String roles;
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return List.of("roles");
    }

}
