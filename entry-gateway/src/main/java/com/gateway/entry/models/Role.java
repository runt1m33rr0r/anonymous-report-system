package com.gateway.entry.models;

public enum Role {
    ADMIN, EMPLOYEE, USER
}