package com.secure.server.services;

import com.secure.server.controllers.models.BundleModel;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.UntrustedIdentityException;

public interface SignalService {

    BundleModel getServerPreKeyBundle() throws InvalidKeyException;

    void saveClientPreKeyBundle(BundleModel bundle) throws Exception;

    String encryptMessage(String sourceName, String sourceMessage)
            throws UntrustedIdentityException, InvalidKeyException;

    String decryptMessage(String sourceName, String sourceMessage) throws Exception;
}
