package com.secure.server.services;

import com.secure.server.entities.Role;

import java.util.Collection;

public interface AuthService {

    boolean isAuthorized(String userToken, Collection<Role> roles);
}
