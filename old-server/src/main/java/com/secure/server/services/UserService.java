package com.secure.server.services;

import com.secure.server.controllers.models.*;

public interface UserService {

    AccessKeysListModel createUsersBatch(int batchSize);

    void createAdminUser();

    ClientResultBundleModel loginUser(ClientBundleModel bundle) throws Exception;

    void changePassword(String userToken, String newPassword);

    AccessKeyModel createEmployee();
}
