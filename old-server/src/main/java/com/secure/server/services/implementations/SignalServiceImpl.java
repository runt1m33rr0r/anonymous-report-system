package com.secure.server.services.implementations;

import com.secure.server.controllers.models.BundleModel;
import com.secure.server.services.SignalService;
import com.secure.server.util.Constants;
import org.springframework.stereotype.Service;
import org.whispersystems.libsignal.*;
import org.whispersystems.libsignal.ecc.Curve;
import org.whispersystems.libsignal.ecc.DjbECPublicKey;
import org.whispersystems.libsignal.ecc.ECKeyPair;
import org.whispersystems.libsignal.ecc.ECPublicKey;
import org.whispersystems.libsignal.protocol.CiphertextMessage;
import org.whispersystems.libsignal.protocol.SignalMessage;
import org.whispersystems.libsignal.ratchet.BobSignalProtocolParameters;
import org.whispersystems.libsignal.ratchet.RatchetingSession;
import org.whispersystems.libsignal.state.SessionRecord;
import org.whispersystems.libsignal.state.SignalProtocolStore;
import org.whispersystems.libsignal.state.impl.InMemorySignalProtocolStore;
import org.whispersystems.libsignal.util.KeyHelper;
import org.whispersystems.libsignal.util.guava.Optional;

import java.lang.reflect.Constructor;
import java.util.Arrays;

@Service
public class SignalServiceImpl implements SignalService {

    private static SignalProtocolStore serverStore;
    private static IdentityKeyPair serverIdentityKey;
    private static ECKeyPair serverBaseKey;

    public BundleModel getServerPreKeyBundle() {
        this.init();

        return new BundleModel(
                Constants.SERVER_NAME,
                ((DjbECPublicKey) serverBaseKey.getPublicKey()).getPublicKey(),
                serverIdentityKey.getPublicKey().serialize());
    }

    public void saveClientPreKeyBundle(BundleModel bundle) throws Exception {
        this.init();

        SignalProtocolAddress clientAddress = new SignalProtocolAddress(bundle.getSourceName(), 1);
        if (serverStore.containsSession(clientAddress)) {
            serverStore.deleteAllSessions(bundle.getSourceName());
            serverStore.saveIdentity(clientAddress, new IdentityKey(bundle.getIdentityPublicKey(), 0));
        }

        ECPublicKey clientBasePublicKey = initECPublicKey(bundle.getBasePublicKey());
        BobSignalProtocolParameters serverParameters = BobSignalProtocolParameters.newBuilder()
                .setOurRatchetKey(serverBaseKey)
                .setOurSignedPreKey(serverBaseKey)
                .setOurOneTimePreKey(Optional.absent())
                .setOurIdentityKey(serverIdentityKey)
                .setTheirIdentityKey(new IdentityKey(bundle.getIdentityPublicKey(), 0))
                .setTheirBaseKey(clientBasePublicKey)
                .create();

        SessionRecord serverSessionRecord = new SessionRecord();
        RatchetingSession.initializeSession(serverSessionRecord.getSessionState(), serverParameters);

        serverStore.storeSession(clientAddress, serverSessionRecord);
    }

    public String encryptMessage(String destinationName, String sourceMessage) throws UntrustedIdentityException {
        this.init();

        SessionCipher serverCipher = new SessionCipher(
                serverStore,
                new SignalProtocolAddress(destinationName, 1));
        CiphertextMessage encryptedMessage = serverCipher.encrypt(sourceMessage.getBytes());

        return Arrays.toString(encryptedMessage.serialize());
    }

    public String decryptMessage(String sourceName, String sourceMessage) throws Exception {
        this.init();

        String[] byteValues = sourceMessage.substring(1, sourceMessage.length() - 1).split(",");
        byte[] bytes = new byte[byteValues.length];
        for (int i = 0, len = bytes.length; i < len; i++) {
            bytes[i] = Byte.parseByte(byteValues[i].trim());
        }

        SessionCipher serverCipher = new SessionCipher(
                serverStore,
                new SignalProtocolAddress(sourceName, 1));
        byte[] decryptedClientMessage = serverCipher.decrypt(new SignalMessage(bytes));

        return new String(decryptedClientMessage);
    }

    private ECPublicKey initECPublicKey(byte[] key) throws Exception {
        Constructor<DjbECPublicKey> constructor = DjbECPublicKey.class.getDeclaredConstructor(byte[].class);
        constructor.setAccessible(true);

        return constructor.newInstance((Object) key);
    }

    private void init() {
        if (serverStore == null) {
            int serverRegistrationId = KeyHelper.generateRegistrationId(false);
            ECKeyPair serverIdentityKeyPair = Curve.generateKeyPair();
            serverIdentityKey = new IdentityKeyPair(
                    new IdentityKey(serverIdentityKeyPair.getPublicKey()),
                    serverIdentityKeyPair.getPrivateKey());
            serverBaseKey = Curve.generateKeyPair();

            serverStore = new InMemorySignalProtocolStore(
                    new IdentityKeyPair(new IdentityKey(
                            serverIdentityKeyPair.getPublicKey()),
                            serverIdentityKeyPair.getPrivateKey()),
                    serverRegistrationId
            );
        }
    }
}
