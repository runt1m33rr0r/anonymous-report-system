package com.secure.server.services.implementations;

import com.secure.server.controllers.models.*;
import com.secure.server.entities.Role;
import com.secure.server.entities.UserEntity;
import com.secure.server.exceptions.ValidationException;
import com.secure.server.repositories.UserRepository;
import com.secure.server.services.SignalService;
import com.secure.server.services.UserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final SignalService signalService;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;

    @Override
    public AccessKeysListModel createUsersBatch(int batchSize) {
        List<UserEntity> usersBatches = new ArrayList<>();
        List<AccessKeyModel> keysBatch = new ArrayList<>();
        for (int i = 0; i < batchSize; i++) {
            String key = UUID.randomUUID().toString();
            usersBatches.add(new UserEntity(key, "", Role.USER));
            keysBatch.add(new AccessKeyModel(key));
        }

        this.userRepository.saveAll(usersBatches);

        return new AccessKeysListModel(keysBatch);
    }

    @Override
    public void createAdminUser() {
        if (this.userRepository.countUsersByRole(Role.ADMIN) == 0) {
            String encodedPassword = this.passwordEncoder.encode("1234");
            this.userRepository.save(new UserEntity(UUID.randomUUID().toString(), encodedPassword, Role.ADMIN));
        }
    }

    @Override
    public ClientResultBundleModel loginUser(ClientBundleModel bundle) throws Exception {
        UserEntity userEntity = this.userRepository.getFirstByToken(bundle.getSourceName());
        if (userEntity != null) {
            if (userEntity.getPassword().equals("")) {
                String encodedPassword = this.passwordEncoder.encode(bundle.getPassword());
                userEntity.setPassword(encodedPassword);
                this.userRepository.save(userEntity);
            } else if (!this.passwordEncoder.matches(bundle.getPassword(), userEntity.getPassword())) {
                throw new ValidationException("Wrong password!");
            }

            BundleModel userMappedBundle = this.modelMapper.map(bundle, BundleModel.class);
            this.signalService.saveClientPreKeyBundle(userMappedBundle);

            ClientResultBundleModel serverMappedBundle = this.modelMapper.map(
                    this.signalService.getServerPreKeyBundle(),
                    ClientResultBundleModel.class);
            serverMappedBundle.setRole(userEntity.getRole());

            return serverMappedBundle;
        } else {
            throw new ValidationException("User does not exist!");
        }
    }

    @Override
    public void changePassword(String userToken, String newPassword) {
        UserEntity userEntity = this.userRepository.getFirstByToken(userToken);
        if (userEntity == null) {
            throw new RuntimeException("User does not exist!");
        }

        String encodedPassword = this.passwordEncoder.encode(newPassword);
        userEntity.setPassword(encodedPassword);
        this.userRepository.save(userEntity);
    }

    @Override
    public AccessKeyModel createEmployee() {
        AccessKeyModel employeeAccessKey = new AccessKeyModel(UUID.randomUUID().toString());
        this.userRepository.save(new UserEntity(employeeAccessKey.getValue(), "", Role.EMPLOYEE));

        return employeeAccessKey;
    }
}
