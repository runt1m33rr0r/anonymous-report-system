package com.secure.server.services.implementations;

import com.secure.server.entities.Role;
import com.secure.server.entities.UserEntity;
import com.secure.server.repositories.UserRepository;
import com.secure.server.services.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;

    public boolean isAuthorized(String userToken, Collection<Role> roles) {
        UserEntity userEntity = this.userRepository.getFirstByToken(userToken);
        if (userEntity == null) {
            return false;
        }

        return roles.contains(userEntity.getRole());
    }
}
