package com.secure.server.services;

import com.secure.server.controllers.models.*;
import com.secure.server.entities.ReportState;

public interface ReportService {

    void createReport(CreateReportModel reportModel);

    ReportsListModel getUserReports(String token, ReportState state, String contains);

    ReportsListModel getReports(ReportState state, String contains);

    CommentsListModel getReportComments(long reportId);

    CommentsListModel getUserReportComments(long reportId, String userToken);

    void addCommentToOwnReport(long reportId, String userToken, CreateCommentModel commentModel);

    void addCommentToReport(long reportId, String userToken, CreateCommentModel commentModel);

    void setReportState(long reportId, ReportState newState);

    ReportModel deleteReport(long reportId, String userToken);
}
