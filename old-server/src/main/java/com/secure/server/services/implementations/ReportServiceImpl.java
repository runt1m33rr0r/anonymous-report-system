package com.secure.server.services.implementations;

import com.secure.server.controllers.models.*;
import com.secure.server.entities.CommentEntity;
import com.secure.server.entities.ReportEntity;
import com.secure.server.entities.ReportState;
import com.secure.server.entities.UserEntity;
import com.secure.server.exceptions.ValidationException;
import com.secure.server.repositories.CommentRepository;
import com.secure.server.repositories.ReportRepository;
import com.secure.server.repositories.UserRepository;
import com.secure.server.services.ReportService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ReportServiceImpl implements ReportService {

    private final UserRepository userRepository;
    private final ReportRepository reportRepository;
    private final CommentRepository commentRepository;
    private final ModelMapper modelMapper;

    @Override
    public void createReport(CreateReportModel reportModel) {
        UserEntity author = this.getUserByToken(reportModel.getAuthor());

        List<CommentEntity> comments = new ArrayList<>();
        CommentEntity commentEntity = new CommentEntity(author, reportModel.getDescription(), LocalDate.now());
        this.commentRepository.save(commentEntity);
        comments.add(commentEntity);
        ReportEntity report = new ReportEntity(author,
                ReportState.OPEN,
                reportModel.getTitle(),
                comments,
                LocalDate.now());
        this.reportRepository.save(report);
    }

    @Override
    public ReportsListModel getUserReports(String token, ReportState state, String contains) {
        UserEntity author = this.getUserByToken(token);

        List<ReportEntity> reports;
        if (state == null) {
            if (contains == null || contains.isBlank()) {
                reports = this.reportRepository.findAllByAuthor_Id(author.getId());
            } else {
                reports = this.reportRepository.findAllByAuthor_IdAndTitleContains(author.getId(), contains);
            }
        } else {
            if (contains == null || contains.isBlank()) {
                reports = this.reportRepository.findAllByAuthor_IdAndState(author.getId(), state);
            } else {
                reports = this.reportRepository.findAllByAuthor_IdAndStateAndTitleContains(author.getId(), state, contains);
            }
        }

        List<ReportModel> mappedReports = reports.stream().map(report ->
                this.modelMapper.map(report, ReportModel.class)).collect(Collectors.toList());

        return new ReportsListModel(mappedReports);
    }

    @Override
    public ReportsListModel getReports(ReportState state, String contains) {
        List<ReportEntity> reports;
        if (state == null) {
            if (contains == null || contains.isBlank()) {
                reports = this.reportRepository.findAll();
            } else {
                reports = this.reportRepository.findAllByTitleContains(contains);
            }
        } else {
            if (contains == null || contains.isBlank()) {
                reports = this.reportRepository.findAllByState(state);
            } else {
                reports = this.reportRepository.findAllByStateAndTitleContains(state, contains);
            }
        }

        List<ReportModel> mappedReports = reports.stream().map(report ->
                this.modelMapper.map(report, ReportModel.class)).collect(Collectors.toList());

        return new ReportsListModel(mappedReports);
    }

    @Override
    public CommentsListModel getReportComments(long reportId) {
        Optional<ReportEntity> report = this.reportRepository.findById(reportId);
        if (report.isEmpty()) {
            throw new ValidationException("Report does not exist!");
        }

        List<CommentModel> mappedComments = this.mapComments(report.get().getComments());

        return new CommentsListModel(mappedComments);
    }

    @Override
    public CommentsListModel getUserReportComments(long reportId, String userToken) {
        ReportEntity report = this.getUserReport(reportId, userToken);
        List<CommentModel> mappedComments = this.mapComments(report.getComments());

        return new CommentsListModel(mappedComments);
    }

    @Override
    public void addCommentToOwnReport(long reportId, String userToken, CreateCommentModel commentModel) {
        ReportEntity report = this.getUserReport(reportId, userToken);
        CommentEntity commentEntity = new CommentEntity(report.getAuthor(), commentModel.getContent(), LocalDate.now());
        this.commentRepository.save(commentEntity);
        report.getComments().add(commentEntity);
        this.reportRepository.save(report);
    }

    @Override
    public void addCommentToReport(long reportId, String userToken, CreateCommentModel commentModel) {
        UserEntity author = this.getUserByToken(userToken);

        Optional<ReportEntity> report = this.reportRepository.findById(reportId);
        if (report.isEmpty()) {
            throw new ValidationException("Report does not exist!");
        }

        CommentEntity commentEntity = new CommentEntity(author, commentModel.getContent(), LocalDate.now());
        this.commentRepository.save(commentEntity);
        report.get().getComments().add(commentEntity);
        this.reportRepository.save(report.get());
    }

    @Override
    public void setReportState(long reportId, ReportState newState) {
        Optional<ReportEntity> reportEntity = this.reportRepository.findById(reportId);
        if (reportEntity.isEmpty()) {
            throw new ValidationException("Report does not exist!");
        }

        if (newState == ReportState.OPEN) {
            throw new ValidationException("Invalid new state!");
        }

        reportEntity.get().setState(newState);
        this.reportRepository.save(reportEntity.get());
    }

    @Override
    public ReportModel deleteReport(long reportId, String userToken) {
        ReportEntity reportEntity = this.getUserReport(reportId, userToken);
        if (reportEntity.getState() != ReportState.DISAPPROVED) {
            throw new ValidationException("Can not delete this report!");
        }

        reportEntity.setDeleted(true);
        this.reportRepository.save(reportEntity);

        return this.modelMapper.map(reportEntity, ReportModel.class);
    }

    private UserEntity getUserByToken(String token) {
        UserEntity user = this.userRepository.getFirstByToken(token);
        if (user == null) {
            throw new ValidationException("User is invalid!");
        }

        return user;
    }

    private ReportEntity getUserReport(long reportId, String userToken) {
        UserEntity author = this.getUserByToken(userToken);
        ReportEntity report = this.reportRepository.findByIdAndAuthor_Id(reportId, author.getId());

        if (report == null) {
            throw new ValidationException("Can not get report!");
        }

        return report;
    }

    private List<CommentModel> mapComments(List<CommentEntity> comments) {
        return comments.stream().map(comment -> {
            CommentModel mappedComment = this.modelMapper.map(comment, CommentModel.class);
            mappedComment.setAuthorType(comment.getAuthor().getRole());

            return mappedComment;
        }).sorted(Comparator.comparing(CommentModel::getDate)).collect(Collectors.toList());
    }
}
