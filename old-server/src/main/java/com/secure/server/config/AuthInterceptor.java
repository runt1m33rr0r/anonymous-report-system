package com.secure.server.config;

import com.secure.server.exceptions.UnauthorizedException;
import com.secure.server.services.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@AllArgsConstructor
@Component
public class AuthInterceptor implements HandlerInterceptor {

    private final AuthService authService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod method = (HandlerMethod) handler;
            if (method.hasMethodAnnotation(Authorized.class)) {
                Authorized authorized = method.getMethodAnnotation(Authorized.class);
                assert authorized != null;

                String requestSource = request.getHeader("auth");
                boolean isAuthorized = requestSource != null &&
                        this.authService.isAuthorized(requestSource, Arrays.asList(authorized.roles()));
                if (!isAuthorized) {
                    throw new UnauthorizedException("Unauthorized!");
                }
            }
        }

        return true;
    }
}
