package com.secure.server.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.secure.server.controllers.models.*;
import com.secure.server.exceptions.RequestReadException;
import com.secure.server.exceptions.UnauthorizedException;
import com.secure.server.services.SignalService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import org.whispersystems.libsignal.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.List;

@ControllerAdvice
@RequiredArgsConstructor
public class SecureAdvice implements ResponseBodyAdvice<Object>, RequestBodyAdvice {

    private final SignalService signalService;

    @Override
    public boolean supports(MethodParameter returnType,
                            Class<? extends HttpMessageConverter<?>> converterType) {
        Type returned = returnType.getGenericParameterType();

        return isSupported(returned);
    }

    @Override
    public boolean supports(MethodParameter methodParameter,
                            Type targetType,
                            Class<? extends HttpMessageConverter<?>> converterType) {
        return isSupported(targetType);
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage,
                                           MethodParameter parameter,
                                           Type targetType,
                                           Class<? extends HttpMessageConverter<?>> converterType) {
        List<String> authHeader = inputMessage.getHeaders().get("auth");
        if (authHeader == null || authHeader.size() == 0) {
            throw new UnauthorizedException("Unauthorized!");
        }

        try {
            String message = new String(inputMessage.getBody().readAllBytes(), StandardCharsets.UTF_8);
            ObjectMapper mapper = new ObjectMapper();
            EncryptedData decoded = mapper.readValue(message, EncryptedData.class);
            String requestSource = authHeader.get(0);
            String decryptedMessage = this.signalService.decryptMessage(requestSource, decoded.getData());
            InputStream decodedStream = new ByteArrayInputStream(decryptedMessage.getBytes());

            return new HttpInputMessage() {
                @Override
                public InputStream getBody() {
                    return decodedStream;
                }

                @Override
                public HttpHeaders getHeaders() {
                    return inputMessage.getHeaders();
                }
            };
        } catch (Exception e) {
            throw new RequestReadException();
        }
    }

    @Override
    public Object beforeBodyWrite(Object body,
                                  MethodParameter returnType,
                                  MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request,
                                  ServerHttpResponse response) {
        List<String> authHeader = request.getHeaders().get("auth");
        if (authHeader == null || authHeader.size() == 0) {
            if (body instanceof ExceptionModel) {
                return body;
            }

            throw new UnauthorizedException("Unauthorized!");
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writeValueAsString(body);
            String requestSource = authHeader.get(0);
            String encryptedMessage = this.signalService.encryptMessage(requestSource, json);

            return new EncryptedData(encryptedMessage);
        } catch (JsonProcessingException | UntrustedIdentityException | InvalidKeyException e) {
            e.printStackTrace();

            throw new UnauthorizedException("Unauthorized!");
        }
    }

    @Override
    public Object afterBodyRead(Object body,
                                HttpInputMessage inputMessage,
                                MethodParameter parameter,
                                Type targetType,
                                Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }

    @Override
    public Object handleEmptyBody(Object body,
                                  HttpInputMessage inputMessage,
                                  MethodParameter parameter,
                                  Type targetType,
                                  Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }

    private static boolean isSupported(Type type) {
        boolean isBundleModel = type.getTypeName().equals(BundleModel.class.getTypeName());
        boolean isClientBundleModel = type.getTypeName().equals(ClientBundleModel.class.getTypeName());
        boolean isClientResultBundleModel = type.getTypeName().equals(ClientResultBundleModel.class.getTypeName());

        return !(isBundleModel || isClientBundleModel || isClientResultBundleModel);
    }
}
