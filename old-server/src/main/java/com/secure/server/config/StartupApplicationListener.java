package com.secure.server.config;

import com.secure.server.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(0)
@AllArgsConstructor
public class StartupApplicationListener implements ApplicationListener<ApplicationReadyEvent> {

    private final UserService userService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        this.userService.createAdminUser();
    }
}
