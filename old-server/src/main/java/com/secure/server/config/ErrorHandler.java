package com.secure.server.config;

import com.secure.server.controllers.models.ExceptionModel;
import com.secure.server.exceptions.RequestReadException;
import com.secure.server.exceptions.UnauthorizedException;
import com.secure.server.exceptions.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ExceptionModel> handleRequestUnauthorizedException(UnauthorizedException exception) {
        return new ResponseEntity<>(new ExceptionModel(exception.getMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(RequestReadException.class)
    public ResponseEntity<ExceptionModel> handleRequestReadException(RequestReadException exception) {
        return new ResponseEntity<>(new ExceptionModel(exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<ExceptionModel> handleValidationException(ValidationException exception) {
        return new ResponseEntity<>(new ExceptionModel(exception.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
