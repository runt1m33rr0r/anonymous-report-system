package com.secure.server.controllers;

import com.secure.server.config.Authorized;
import com.secure.server.controllers.models.*;
import com.secure.server.entities.Role;
import com.secure.server.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/keys/{count}")
    @Authorized(roles = { Role.ADMIN })
    public AccessKeysListModel getAccessKeys(@PathVariable("count") int count) {
        return this.userService.createUsersBatch(count);
    }

    @PostMapping("/login")
    public ClientResultBundleModel login(@RequestBody @Valid ClientBundleModel bundleModel) throws Exception {
        return this.userService.loginUser(bundleModel);
    }

    @PostMapping("/password")
    @Authorized(roles = { Role.USER, Role.ADMIN, Role.EMPLOYEE })
    public void changePassword(@RequestHeader("auth") String userToken,
                               @RequestBody @Valid UpdatePasswordModel updatePasswordModel) {
        this.userService.changePassword(userToken, updatePasswordModel.getPassword());
    }

    @GetMapping("/employee")
    @Authorized(roles = { Role.ADMIN })
    public AccessKeyModel createEmployee() {
        return this.userService.createEmployee();
    }
}
