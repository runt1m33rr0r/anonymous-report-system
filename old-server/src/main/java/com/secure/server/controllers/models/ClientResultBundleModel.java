package com.secure.server.controllers.models;

import com.secure.server.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientResultBundleModel {
    private String sourceName;
    private byte[] basePublicKey;
    private byte[] identityPublicKey;
    private Role role;
}