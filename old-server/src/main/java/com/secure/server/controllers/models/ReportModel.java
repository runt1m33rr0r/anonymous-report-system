package com.secure.server.controllers.models;

import com.secure.server.entities.ReportState;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReportModel {
    private long id;
    private String title;
    private String date;
    private ReportState state;
}
