package com.secure.server.controllers;

import com.secure.server.config.Authorized;
import com.secure.server.controllers.models.*;
import com.secure.server.entities.ReportState;
import com.secure.server.entities.Role;
import com.secure.server.services.ReportService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/reports")
@AllArgsConstructor
public class ReportController {

    private final ReportService reportService;

    @PostMapping()
    @Authorized(roles = { Role.USER })
    public CreateReportModel createReport(@RequestBody @Valid CreateReportModel createReportModel) {
        this.reportService.createReport(createReportModel);

        return createReportModel;
    }

    @GetMapping("/mine")
    @Authorized(roles = { Role.USER })
    public ReportsListModel getUserReports(
            @RequestHeader("auth") String userToken,
            @RequestParam(required = false, name = "state") String state,
            @RequestParam(required = false, name = "contains") String contains) {
        ReportState reportState = null;
        try {
            reportState = ReportState.valueOf(state);
        } catch (IllegalArgumentException | NullPointerException ignored) { }

        return this.reportService.getUserReports(userToken, reportState, contains);
    }

    @GetMapping("/mine/{id}")
    @Authorized(roles = { Role.USER })
    public CommentsListModel getUserReportComments(@RequestHeader("auth") String userToken,
                                                   @PathVariable("id") long id) {
        return this.reportService.getUserReportComments(id, userToken);
    }

    @PostMapping("/mine/{id}/comments")
    @Authorized(roles = { Role.USER })
    public CommentsListModel addCommentToMyReport(@RequestHeader("auth") String userToken,
                                                  @RequestBody @Valid CreateCommentModel createCommentModel,
                                                  @PathVariable("id") long reportId) {
        this.reportService.addCommentToOwnReport(reportId, userToken, createCommentModel);

        return this.reportService.getUserReportComments(reportId, userToken);
    }

    @GetMapping()
    @Authorized(roles = { Role.EMPLOYEE })
    public ReportsListModel getReports(
            @RequestParam(required = false, name = "state") String state,
            @RequestParam(required = false, name = "contains") String contains) {
        ReportState reportState = null;
        try {
            reportState = ReportState.valueOf(state);
        } catch (IllegalArgumentException | NullPointerException ignored) { }

        return this.reportService.getReports(reportState, contains);
    }

    @GetMapping("/{id}")
    @Authorized(roles = { Role.EMPLOYEE })
    public CommentsListModel getReportComments(@PathVariable("id") long id) {
        return this.reportService.getReportComments(id);
    }

    @PostMapping("/{id}/comments")
    @Authorized(roles = { Role.EMPLOYEE })
    public CommentsListModel addCommentToReport(@RequestHeader("auth") String userToken,
                                                @RequestBody @Valid CreateCommentModel createCommentModel,
                                                @PathVariable("id") long reportId) {
        this.reportService.addCommentToReport(reportId, userToken, createCommentModel);

        return this.reportService.getReportComments(reportId);
    }

    @PutMapping("/{id}")
    @Authorized(roles = { Role.EMPLOYEE })
    public UpdateReportStateModel updateReportState(@PathVariable("id") long reportId,
                                                    @RequestBody @Valid UpdateReportStateModel updateReportStateModel) {
        this.reportService.setReportState(reportId, updateReportStateModel.getNewState());

        return updateReportStateModel;
    }

    @DeleteMapping("/mine/{id}")
    @Authorized(roles = { Role.USER })
    public ReportModel deleteReport(@RequestHeader("auth") String userToken, @PathVariable("id") long reportId) {
        return this.reportService.deleteReport(reportId, userToken);
    }
}
