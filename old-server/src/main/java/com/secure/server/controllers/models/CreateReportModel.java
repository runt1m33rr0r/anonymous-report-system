package com.secure.server.controllers.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateReportModel {

    @Length(min = 3)
    private String author;

    @Length(min = 3, max = 300)
    private String title;

    @Length(min = 3)
    private String description;
}
