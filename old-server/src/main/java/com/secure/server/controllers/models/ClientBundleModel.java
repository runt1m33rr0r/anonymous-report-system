package com.secure.server.controllers.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientBundleModel {

    @Length(min = 3)
    private String sourceName;

    @Length(min = 3)
    private String password;

    @NotNull
    private byte[] basePublicKey;

    @NotNull
    private byte[] identityPublicKey;
}
