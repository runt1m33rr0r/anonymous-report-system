package com.secure.server.repositories;

import com.secure.server.entities.ReportEntity;
import com.secure.server.entities.ReportState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportRepository extends JpaRepository<ReportEntity, Long> {
    List<ReportEntity> findAllByAuthor_Id(long id);
    List<ReportEntity> findAllByAuthor_IdAndState(long id, ReportState state);
    List<ReportEntity> findAllByAuthor_IdAndTitleContains(long id, String contains);
    List<ReportEntity> findAllByAuthor_IdAndStateAndTitleContains(long id, ReportState state, String contains);
    List<ReportEntity> findAllByTitleContains(String contains);
    List<ReportEntity> findAllByState(ReportState state);
    List<ReportEntity> findAllByStateAndTitleContains(ReportState state, String contains);
    ReportEntity findByIdAndAuthor_Id(long id, long authorId);
}
