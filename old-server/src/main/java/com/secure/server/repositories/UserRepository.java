package com.secure.server.repositories;

import com.secure.server.entities.Role;
import com.secure.server.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity getFirstByToken(String token);

    int countUsersByRole(Role role);
}
