package com.secure.server.exceptions;

public class RequestReadException extends RuntimeException {
    public RequestReadException() {
        super("Could not read request!");
    }
}
