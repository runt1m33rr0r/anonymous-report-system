package com.secure.server.entities;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "is_deleted='false'")
public class UserEntity extends BaseEntity {

    @NotNull
    private String token;

    @NotNull
    private String password;

    @NotNull
    private Role role;
}
