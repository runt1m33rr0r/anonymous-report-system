package com.secure.server.entities;

public enum ReportState {
    OPEN, APPROVED, DISAPPROVED, IN_PROGRESS, RESOLVED
}
