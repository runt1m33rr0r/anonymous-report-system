package com.secure.server.entities;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "is_deleted='false'")
public class CommentEntity extends BaseEntity {

    @NotNull
    @ManyToOne
    private UserEntity author;

    @Length(min = 3)
    private String content;

    @NotNull
    private LocalDate date;
}
