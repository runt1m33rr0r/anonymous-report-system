package com.secure.server.entities;

public enum Role {
    ADMIN, EMPLOYEE, USER
}
