package com.secure.server.entities;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "is_deleted='false'")
public class ReportEntity extends BaseEntity {

    @NotNull
    @ManyToOne
    private UserEntity author;

    @NotNull
    private ReportState state;

    @Length(min = 3)
    private String title;

    @NotNull
    @OneToMany
    private List<CommentEntity> comments;

    @NotNull
    private LocalDate date;
}
