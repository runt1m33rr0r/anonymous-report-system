package com.secure.server.controllers;

import com.secure.server.config.AuthInterceptor;
import com.secure.server.config.SecureAdvice;
import com.secure.server.controllers.models.AccessKeyModel;
import com.secure.server.controllers.models.AccessKeysListModel;
import com.secure.server.controllers.models.ClientBundleModel;
import com.secure.server.controllers.models.ClientResultBundleModel;
import com.secure.server.entities.Role;
import com.secure.server.services.AuthService;
import com.secure.server.services.SignalService;
import com.secure.server.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
class UserControllerTest {

    @MockBean
    private UserService userService;

    @MockBean
    private AuthService authService;

    @MockBean
    private SignalService signalService;

    @MockBean
    private SecureAdvice secureAdvice;

    @MockBean
    private AuthInterceptor authInterceptor;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserController userController;

    @Test
    void getAccessKeys() throws Exception {
        int batchSize = 1;
        List<AccessKeyModel> accessKeys = new ArrayList<>();
        accessKeys.add(new AccessKeyModel("key1"));
        AccessKeysListModel accessKeysListModel = new AccessKeysListModel(accessKeys);
        when(this.userService.createUsersBatch(batchSize)).thenReturn(accessKeysListModel);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/users/keys/" + batchSize))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getAccessKeys_method() {
        int batchSize = 5;
        when(this.userService.createUsersBatch(batchSize)).thenReturn(new AccessKeysListModel());

        AccessKeysListModel result = this.userController.getAccessKeys(batchSize);

        assertThat(result).isNotNull();
    }

    @Test
    void login() throws Exception {
        ClientBundleModel clientBundleModel = new ClientBundleModel(
                "source",
                "password",
                new byte[] {},
                new byte[] {});
        ClientResultBundleModel resultBundleModel = new ClientResultBundleModel(
                "source",
                new byte[] {},
                new byte[] {},
                Role.USER);
        when(this.userService.loginUser(clientBundleModel)).thenReturn(resultBundleModel);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/users/login"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void createEmployee() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/users/employee"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void createEmployee_method() {
        when(this.userService.createEmployee()).thenReturn(new AccessKeyModel());

        AccessKeyModel accessKeyModel = this.userController.createEmployee();

        assertThat(accessKeyModel).isNotNull();
    }

    @Test
    void changePassword() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/users/password"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
