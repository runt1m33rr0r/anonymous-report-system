package com.secure.server.controllers;

import com.secure.server.config.AuthInterceptor;
import com.secure.server.config.SecureAdvice;
import com.secure.server.controllers.models.CreateReportModel;
import com.secure.server.services.AuthService;
import com.secure.server.services.ReportService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ReportController.class)
class ReportControllerTest {

    @MockBean
    private ReportService reportService;

    @MockBean
    private AuthService authService;

    @MockBean
    private AuthInterceptor authInterceptor;

    @MockBean
    private SecureAdvice secureAdvice;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void createReport() throws Exception {
        CreateReportModel createReportModel = new CreateReportModel(
                "author",
                "title",
                "description");

        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/reports", createReportModel))
                .andDo(print())
                .andExpect(status().isOk());
    }
}