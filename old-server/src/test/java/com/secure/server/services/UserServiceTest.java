package com.secure.server.services;

import com.secure.server.controllers.models.AccessKeyModel;
import com.secure.server.controllers.models.AccessKeysListModel;
import com.secure.server.controllers.models.ClientBundleModel;
import com.secure.server.controllers.models.ClientResultBundleModel;
import com.secure.server.entities.Role;
import com.secure.server.entities.UserEntity;
import com.secure.server.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private SignalService signalService;

    @MockBean
    private ModelMapper modelMapper;

    @Autowired
    private UserService userService;

    @Test
    void testCreateUsersBatch() {
        int batchSize = 1;

        AccessKeysListModel keys = this.userService.createUsersBatch(batchSize);

        assertThat(keys.getKeys().size()).isEqualTo(1);
    }

    @Test
    void testCreateAdminUser() {
        this.userService.createAdminUser();

        assertThatNoException();
    }

    @Test
    void testCreateEmployee() {
        AccessKeyModel accessKeyModel = this.userService.createEmployee();

        assertThat(accessKeyModel).isNotNull();
    }

    @Test
    void testLoginUser_userDoesNotExist() {
        String token = "token";
        ClientBundleModel clientBundleModel = new ClientBundleModel(
                token,
                "password",
                new byte[] {},
                new byte[] {});
        when(this.userRepository.getFirstByToken(token)).thenReturn(null);

        assertThatThrownBy(() -> this.userService.loginUser(clientBundleModel), "User does not exist!");
    }

    @Test
    void testLoginUser_passwordDoesNotMatch() {
        String token = "token";
        String password = "password";
        ClientBundleModel clientBundleModel = new ClientBundleModel(
                token,
                password,
                new byte[] {},
                new byte[] {});
        UserEntity userEntity = new UserEntity(token, password, Role.USER);
        when(this.userRepository.getFirstByToken(token)).thenReturn(userEntity);
        when(this.passwordEncoder.matches(password, password)).thenReturn(false);

        assertThatThrownBy(() -> this.userService.loginUser(clientBundleModel), "Wrong password!");
    }

    @Test
    void testLoginUser() throws Exception {
        String token = "token";
        String password = "password";
        ClientBundleModel clientBundleModel = new ClientBundleModel(
                token,
                password,
                new byte[] {},
                new byte[] {});
        UserEntity userEntity = new UserEntity(token, password, Role.USER);
        when(this.userRepository.getFirstByToken(token)).thenReturn(userEntity);
        when(this.passwordEncoder.matches(password, password)).thenReturn(true);
        when(this.modelMapper.map(null, ClientResultBundleModel.class)).thenReturn(new ClientResultBundleModel());

        ClientResultBundleModel clientResultBundleModel = this.userService.loginUser(clientBundleModel);

        assertThat(clientResultBundleModel).isNotNull();
    }

    @Test
    void testLoginUser_initialLogin() throws Exception {
        String token = "token";
        String password = "password";
        ClientBundleModel clientBundleModel = new ClientBundleModel(
                token,
                "password",
                new byte[] {},
                new byte[] {});
        UserEntity userEntity = new UserEntity(token, "", Role.USER);
        when(this.userRepository.getFirstByToken(token)).thenReturn(userEntity);
        when(this.passwordEncoder.matches(password, password)).thenReturn(true);
        when(this.modelMapper.map(null, ClientResultBundleModel.class)).thenReturn(new ClientResultBundleModel());

        ClientResultBundleModel clientResultBundleModel = this.userService.loginUser(clientBundleModel);

        assertThat(clientResultBundleModel).isNotNull();
    }

    @Test
    void testChangePassword() {
        String token = "token";
        String password = "password";
        UserEntity userEntity = new UserEntity(token, password, Role.USER);
        when(this.userRepository.getFirstByToken(token)).thenReturn(userEntity);

        this.userService.changePassword(token, password);

        assertThatNoException();
    }

    @Test
    void testChangePassword_userDoesNotExist() {
        String token = "token";
        String password = "password";
        when(this.userRepository.getFirstByToken(token)).thenReturn(null);

        assertThatThrownBy(() -> this.userService.changePassword(token, password), "User does not exist!");
    }
}
