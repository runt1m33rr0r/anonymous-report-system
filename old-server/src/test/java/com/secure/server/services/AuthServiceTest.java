package com.secure.server.services;

import com.secure.server.entities.Role;
import com.secure.server.entities.UserEntity;
import com.secure.server.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    @Test
    void testIsAuthorized() {
        String userToken = "token";
        UserEntity userEntity = new UserEntity(userToken, "password", Role.USER);
        when(this.userRepository.getFirstByToken(userToken)).thenReturn(userEntity);

        boolean isAuthorized = this.authService.isAuthorized(userToken, Collections.singletonList(Role.USER));

        assertThat(isAuthorized).isEqualTo(true);
    }

    @Test
    void testIsAuthorized_missingUser() {
        String userToken = "token";
        when(this.userRepository.getFirstByToken(userToken)).thenReturn(null);

        boolean isAuthorized = this.authService.isAuthorized(userToken, Collections.singletonList(Role.USER));

        assertThat(isAuthorized).isEqualTo(false);
    }

    @Test
    void testIsAuthorized_userIsNotInRole() {
        String userToken = "token";
        UserEntity userEntity = new UserEntity(userToken, "password", Role.USER);
        when(this.userRepository.getFirstByToken(userToken)).thenReturn(userEntity);

        boolean isAuthorized = this.authService.isAuthorized(userToken, Collections.singletonList(Role.ADMIN));

        assertThat(isAuthorized).isEqualTo(false);
    }
}
