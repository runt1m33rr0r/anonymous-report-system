package com.secure.server.services;

import com.secure.server.controllers.models.*;
import com.secure.server.entities.*;
import com.secure.server.repositories.CommentRepository;
import com.secure.server.repositories.ReportRepository;
import com.secure.server.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ReportServiceTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ReportRepository reportRepository;

    @MockBean
    private CommentRepository commentRepository;

    @MockBean
    private ModelMapper modelMapper;

    @Autowired
    private ReportService reportService;

    @Test
    void testCreateReport() {
        String authorToken = "token";
        CreateReportModel createReportModel = new CreateReportModel(authorToken, "title", "description");
        UserEntity userEntity = new UserEntity(authorToken, "password", Role.USER);
        when(this.userRepository.getFirstByToken(authorToken)).thenReturn(userEntity);

        this.reportService.createReport(createReportModel);

        assertThatNoException();
    }

    @Test
    void testGetUserReports() {
        String authorToken = "token";
        ReportState reportState = ReportState.OPEN;
        String titleContains = "title";
        UserEntity userEntity = new UserEntity(authorToken, "password", Role.USER);
        when(this.userRepository.getFirstByToken(authorToken)).thenReturn(userEntity);
        when(this.reportRepository.findAllByAuthor_IdAndStateAndTitleContains(
                userEntity.getId(),
                reportState,
                titleContains)).thenReturn(new ArrayList<>());

        ReportsListModel reportsListModel = this.reportService.getUserReports(authorToken, reportState, titleContains);

        assertThat(reportsListModel).isNotNull();
        assertThat(reportsListModel.getReports().size()).isEqualTo(0);
    }

    @Test
    void testGetUserReports_noStateNoContains() {
        String authorToken = "token";
        UserEntity userEntity = new UserEntity(authorToken, "password", Role.USER);
        when(this.userRepository.getFirstByToken(authorToken)).thenReturn(userEntity);
        when(this.reportRepository.findAllByAuthor_Id(userEntity.getId())).thenReturn(new ArrayList<>());

        ReportsListModel reportsListModel = this.reportService.getUserReports(authorToken, null, null);

        assertThat(reportsListModel).isNotNull();
        assertThat(reportsListModel.getReports().size()).isEqualTo(0);
    }

    @Test
    void testGetUserReports_noStateNoContains_userInvalid() {
        String authorToken = "token";
        UserEntity userEntity = new UserEntity(authorToken, "password", Role.USER);
        when(this.userRepository.getFirstByToken(authorToken)).thenReturn(null);
        when(this.reportRepository.findAllByAuthor_Id(userEntity.getId())).thenReturn(new ArrayList<>());

        assertThatThrownBy(
                () -> this.reportService.getUserReports(authorToken, null, null),
                "User is invalid!");
    }

    @Test
    void testGetUserReports_noStateContains() {
        String authorToken = "token";
        UserEntity userEntity = new UserEntity(authorToken, "password", Role.USER);
        when(this.userRepository.getFirstByToken(authorToken)).thenReturn(userEntity);
        when(this.reportRepository.findAllByAuthor_Id(userEntity.getId())).thenReturn(new ArrayList<>());

        ReportsListModel reportsListModel = this.reportService.getUserReports(authorToken, null, "title");

        assertThat(reportsListModel).isNotNull();
        assertThat(reportsListModel.getReports().size()).isEqualTo(0);
    }

    @Test
    void testGetUserReports_stateNoContains() {
        String authorToken = "token";
        UserEntity userEntity = new UserEntity(authorToken, "password", Role.USER);
        when(this.userRepository.getFirstByToken(authorToken)).thenReturn(userEntity);
        when(this.reportRepository.findAllByAuthor_Id(userEntity.getId())).thenReturn(new ArrayList<>());

        ReportsListModel reportsListModel = this.reportService.getUserReports(
                authorToken,
                ReportState.OPEN,
                null);

        assertThat(reportsListModel).isNotNull();
        assertThat(reportsListModel.getReports().size()).isEqualTo(0);
    }

    @Test
    void testGetReports() {
        String contains = "test";
        ReportState state = ReportState.OPEN;
        when(this.reportRepository.findAllByStateAndTitleContains(state, contains)).thenReturn(new ArrayList<>());

        ReportsListModel reportsListModel = this.reportService.getReports(state, contains);

        assertThat(reportsListModel).isNotNull();
        assertThat(reportsListModel.getReports().size()).isEqualTo(0);
    }

    @Test
    void testGetReports_noStateNoContains() {
        when(this.reportRepository.findAll()).thenReturn(new ArrayList<>());

        ReportsListModel reportsListModel = this.reportService.getReports(null, null);

        assertThat(reportsListModel).isNotNull();
        assertThat(reportsListModel.getReports().size()).isEqualTo(0);
    }

    @Test
    void testGetReports_stateNoContains() {
        ReportState state = ReportState.OPEN;
        when(this.reportRepository.findAllByStateAndTitleContains(state, null)).thenReturn(new ArrayList<>());

        ReportsListModel reportsListModel = this.reportService.getReports(state, null);

        assertThat(reportsListModel).isNotNull();
        assertThat(reportsListModel.getReports().size()).isEqualTo(0);
    }

    @Test
    void testGetReports_noStateContains() {
        String contains = "test";
        when(this.reportRepository.findAllByStateAndTitleContains(null, contains)).thenReturn(new ArrayList<>());

        ReportsListModel reportsListModel = this.reportService.getReports(null, contains);

        assertThat(reportsListModel).isNotNull();
        assertThat(reportsListModel.getReports().size()).isEqualTo(0);
    }

    @Test
    void testGetReportComments_reportEmpty() {
        long reportId = 1;
        when(this.reportRepository.findById(reportId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> this.reportService.getReportComments(reportId));
    }

    @Test
    void testGetReportComments() {
        long reportId = 1;
        ReportEntity reportEntity = new ReportEntity();
        reportEntity.setComments(new ArrayList<>());
        Optional<ReportEntity> reportEntityOptional = Optional.of(reportEntity);
        when(this.reportRepository.findById(reportId)).thenReturn(reportEntityOptional);

        CommentsListModel result = this.reportService.getReportComments(reportId);

        assertThat(result.getComments().size()).isEqualTo(0);
    }

    @Test
    void testGetUserReportComments() {
        long reportId = 1;
        String userToken = "token";
        UserEntity author = new UserEntity(userToken, "password", Role.USER);
        ReportEntity report = new ReportEntity();
        CommentEntity commentEntity = new CommentEntity(author, "content", LocalDate.now());
        report.setComments(Collections.singletonList(commentEntity));
        when(this.userRepository.getFirstByToken(userToken)).thenReturn(author);
        when(this.reportRepository.findByIdAndAuthor_Id(reportId, author.getId())).thenReturn(report);
        when(this.modelMapper.map(commentEntity, CommentModel.class)).thenReturn(new CommentModel());

        CommentsListModel result = this.reportService.getUserReportComments(reportId, userToken);

        assertThat(result.getComments().size()).isEqualTo(1);
    }

    @Test
    void testGetUserReportComments_canNotGetReport() {
        long reportId = 1;
        String userToken = "token";
        UserEntity author = new UserEntity(userToken, "password", Role.USER);
        ReportEntity report = new ReportEntity();
        report.setComments(new ArrayList<>());
        when(this.userRepository.getFirstByToken(userToken)).thenReturn(author);
        when(this.reportRepository.findByIdAndAuthor_Id(reportId, author.getId())).thenReturn(null);

        assertThatThrownBy(
                () -> this.reportService.getUserReportComments(reportId, userToken),
                "Can not get report!");
    }

    @Test
    void testAddCommentToOwnReport() {
        long reportId = 1;
        String userToken = "token";
        UserEntity author = new UserEntity(userToken, "password", Role.USER);
        ReportEntity report = new ReportEntity();
        CreateCommentModel newComment = new CreateCommentModel("content");
        report.setComments(new ArrayList<>());
        when(this.userRepository.getFirstByToken(userToken)).thenReturn(author);
        when(this.reportRepository.findByIdAndAuthor_Id(reportId, author.getId())).thenReturn(report);

        this.reportService.addCommentToOwnReport(reportId, userToken, newComment);

        assertThat(report.getComments().size()).isEqualTo(1);
    }

    @Test
    void testAddCommentToReport() {
        long reportId = 1;
        String userToken = "token";
        UserEntity author = new UserEntity(userToken, "password", Role.USER);
        ReportEntity report = new ReportEntity();
        CreateCommentModel newComment = new CreateCommentModel("content");
        report.setComments(new ArrayList<>());
        when(this.userRepository.getFirstByToken(userToken)).thenReturn(author);
        when(this.reportRepository.findById(reportId)).thenReturn(Optional.of(report));

        this.reportService.addCommentToReport(reportId, userToken, newComment);

        assertThat(report.getComments().size()).isEqualTo(1);
    }

    @Test
    void testAddCommentToReport_reportNotExist() {
        long reportId = 1;
        String userToken = "token";
        UserEntity author = new UserEntity(userToken, "password", Role.USER);
        ReportEntity report = new ReportEntity();
        CreateCommentModel newComment = new CreateCommentModel("content");
        report.setComments(new ArrayList<>());
        when(this.userRepository.getFirstByToken(userToken)).thenReturn(author);
        when(this.reportRepository.findById(reportId)).thenReturn(Optional.empty());

        assertThatThrownBy(
                () -> this.reportService.addCommentToReport(reportId, userToken, newComment),
                "Report does not exist!");
    }

    @Test
    void testSetReportState() {
        long reportId = 1;
        ReportState newState = ReportState.RESOLVED;
        ReportEntity report = new ReportEntity();
        when(this.reportRepository.findById(reportId)).thenReturn(Optional.of(report));

        this.reportService.setReportState(reportId, newState);

        assertThat(report.getState()).isEqualTo(newState);
    }

    @Test
    void testSetReportState_reportNotExist() {
        long reportId = 1;
        ReportState newState = ReportState.RESOLVED;
        when(this.reportRepository.findById(reportId)).thenReturn(Optional.empty());

        assertThatThrownBy(
                () -> this.reportService.setReportState(reportId, newState),
                "Report does not exist!");
    }

    @Test
    void testSetReportState_invalidNewState() {
        long reportId = 1;
        ReportState newState = ReportState.OPEN;
        ReportEntity report = new ReportEntity();
        when(this.reportRepository.findById(reportId)).thenReturn(Optional.of(report));

        assertThatThrownBy(
                () -> this.reportService.setReportState(reportId, newState),
                "Invalid new state!");
    }

    @Test
    void testDeleteReport() {
        long reportId = 1;
        String token = "token";
        UserEntity author = new UserEntity(token, "password", Role.USER);
        ReportEntity report = new ReportEntity();
        report.setState(ReportState.DISAPPROVED);
        when(this.userRepository.getFirstByToken(token)).thenReturn(author);
        when(this.reportRepository.findByIdAndAuthor_Id(reportId, author.getId())).thenReturn(report);

        this.reportService.deleteReport(reportId, token);

        assertThat(report.isDeleted()).isEqualTo(true);
    }

    @Test
    void testDeleteReport_canNotDelete() {
        long reportId = 1;
        String token = "token";
        UserEntity author = new UserEntity(token, "password", Role.USER);
        ReportEntity report = new ReportEntity();
        report.setState(ReportState.OPEN);
        when(this.userRepository.getFirstByToken(token)).thenReturn(author);
        when(this.reportRepository.findByIdAndAuthor_Id(reportId, author.getId())).thenReturn(report);

        assertThatThrownBy(
                () -> this.reportService.deleteReport(reportId, token),
                "Can not delete this report!");
    }
}
