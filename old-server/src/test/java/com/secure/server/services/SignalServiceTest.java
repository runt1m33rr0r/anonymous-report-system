package com.secure.server.services;

import com.secure.server.controllers.models.BundleModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.whispersystems.libsignal.IdentityKey;
import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.ecc.Curve;
import org.whispersystems.libsignal.ecc.DjbECPublicKey;
import org.whispersystems.libsignal.ecc.ECKeyPair;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class SignalServiceTest {

    @Autowired
    private SignalService signalService;

    @Test
    void testGetServerPreKeyBundle() throws InvalidKeyException {
        BundleModel bundle = this.signalService.getServerPreKeyBundle();

        assertThat(bundle).isNotEqualTo(null);
    }

    @Test
    void testSaveClientPreKeyBundle() throws Exception {
        ECKeyPair identityKeyPair = Curve.generateKeyPair();
        byte[] basePublicKey = ((DjbECPublicKey) Curve.generateKeyPair().getPublicKey()).getPublicKey();
        byte[] identityPublicKey = new IdentityKeyPair(
                new IdentityKey(identityKeyPair.getPublicKey()),
                identityKeyPair.getPrivateKey()).getPublicKey().serialize();
        BundleModel bundleModel = new BundleModel("source", basePublicKey, identityPublicKey);

        this.signalService.saveClientPreKeyBundle(bundleModel);

        assertThatNoException();
    }

    @Test
    void testEncryptMessage() throws Exception {
        String destination = "source";
        ECKeyPair identityKeyPair = Curve.generateKeyPair();
        byte[] basePublicKey = ((DjbECPublicKey) Curve.generateKeyPair().getPublicKey()).getPublicKey();
        byte[] identityPublicKey = new IdentityKeyPair(
                new IdentityKey(identityKeyPair.getPublicKey()),
                identityKeyPair.getPrivateKey()).getPublicKey().serialize();
        BundleModel bundleModel = new BundleModel(destination, basePublicKey, identityPublicKey);

        this.signalService.saveClientPreKeyBundle(bundleModel);
        this.signalService.encryptMessage(destination, "message");

        assertThatNoException();
    }

    @Test
    void testEncryptMessage_noSession() throws Exception {
        String destination = "source";
        ECKeyPair identityKeyPair = Curve.generateKeyPair();
        byte[] basePublicKey = ((DjbECPublicKey) Curve.generateKeyPair().getPublicKey()).getPublicKey();
        byte[] identityPublicKey = new IdentityKeyPair(
                new IdentityKey(identityKeyPair.getPublicKey()),
                identityKeyPair.getPrivateKey()).getPublicKey().serialize();
        BundleModel bundleModel = new BundleModel(destination, basePublicKey, identityPublicKey);

        this.signalService.saveClientPreKeyBundle(bundleModel);
        String encrypted = this.signalService.encryptMessage(destination, "message");

        assertThatThrownBy(() -> this.signalService.decryptMessage(destination, encrypted));
    }
}
