package com.secure.server.repositories;

import com.secure.server.entities.Role;
import com.secure.server.entities.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    void getFirstByToken() {
        String token = "token";
        UserEntity userEntity = new UserEntity(token, "password", Role.USER);
        this.testEntityManager.persistAndFlush(userEntity);

        UserEntity receivedEntity = this.userRepository.getFirstByToken(token);

        assertThat(receivedEntity.getPassword()).isEqualTo(userEntity.getPassword());
        assertThat(receivedEntity.getRole()).isEqualTo(userEntity.getRole());
        assertThat(receivedEntity.getToken()).isEqualTo(userEntity.getToken());
    }

    @Test
    void countUsersByRole() {
        UserEntity userEntity1 = new UserEntity("token1", "password", Role.USER);
        UserEntity userEntity2 = new UserEntity("token2", "password", Role.ADMIN);
        UserEntity userEntity3 = new UserEntity("token3", "password", Role.USER);
        this.testEntityManager.persistAndFlush(userEntity1);
        this.testEntityManager.persistAndFlush(userEntity2);
        this.testEntityManager.persistAndFlush(userEntity3);

        int usersCount = this.userRepository.countUsersByRole(Role.USER);
        assertThat(usersCount).isEqualTo(2);
    }
}
