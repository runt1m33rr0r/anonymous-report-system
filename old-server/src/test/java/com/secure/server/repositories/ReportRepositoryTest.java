package com.secure.server.repositories;

import com.secure.server.entities.ReportEntity;
import com.secure.server.entities.ReportState;
import com.secure.server.entities.Role;
import com.secure.server.entities.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ReportRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ReportRepository reportRepository;

    @Test
    void findAllByAuthor_Id() {
        UserEntity author1 = new UserEntity("token1", "password", Role.USER);
        UserEntity author2 = new UserEntity("token2", "password", Role.USER);
        this.testEntityManager.persistAndFlush(author1);
        this.testEntityManager.persistAndFlush(author2);
        ReportEntity reportEntity1 = new ReportEntity();
        reportEntity1.setAuthor(author1);
        ReportEntity reportEntity2 = new ReportEntity();
        reportEntity2.setAuthor(author2);
        this.testEntityManager.persistAndFlush(reportEntity1);
        this.testEntityManager.persistAndFlush(reportEntity2);

        List<ReportEntity> reports = this.reportRepository.findAllByAuthor_Id(author1.getId());

        assertThat(reports.size()).isEqualTo(1);
        assertThat(reports.get(0).getAuthor().getId()).isEqualTo(author1.getId());
    }

    @Test
    void findAllByAuthor_IdAndState() {
        UserEntity author1 = new UserEntity("token1", "password", Role.USER);
        this.testEntityManager.persistAndFlush(author1);
        this.testEntityManager.persistAndFlush(author1);
        ReportEntity reportEntity1 = new ReportEntity();
        reportEntity1.setAuthor(author1);
        reportEntity1.setState(ReportState.APPROVED);
        ReportEntity reportEntity2 = new ReportEntity();
        reportEntity2.setAuthor(author1);
        reportEntity2.setState(ReportState.DISAPPROVED);
        this.testEntityManager.persistAndFlush(reportEntity1);
        this.testEntityManager.persistAndFlush(reportEntity2);

        List<ReportEntity> reports = this.reportRepository.findAllByAuthor_IdAndState(
                author1.getId(),
                ReportState.APPROVED);

        assertThat(reports.size()).isEqualTo(1);
        assertThat(reports.get(0).getAuthor().getId()).isEqualTo(author1.getId());
        assertThat(reports.get(0).getState()).isEqualTo(ReportState.APPROVED);
    }

    @Test
    void findAllByAuthor_IdAndTitleContains() {
        UserEntity author1 = new UserEntity("token1", "password", Role.USER);
        this.testEntityManager.persistAndFlush(author1);
        this.testEntityManager.persistAndFlush(author1);
        ReportEntity reportEntity1 = new ReportEntity();
        reportEntity1.setAuthor(author1);
        reportEntity1.setTitle("some title");
        ReportEntity reportEntity2 = new ReportEntity();
        reportEntity2.setAuthor(author1);
        reportEntity2.setTitle("other title");
        this.testEntityManager.persistAndFlush(reportEntity1);
        this.testEntityManager.persistAndFlush(reportEntity2);

        List<ReportEntity> reports = this.reportRepository.findAllByAuthor_IdAndTitleContains(
                author1.getId(),
                "other");

        assertThat(reports.size()).isEqualTo(1);
        assertThat(reports.get(0).getAuthor().getId()).isEqualTo(author1.getId());
        assertThat(reports.get(0).getTitle()).isEqualTo(reportEntity2.getTitle());
    }

    @Test
    void findAllByAuthor_IdAndStateAndTitleContains() {
        UserEntity author1 = new UserEntity("token1", "password", Role.USER);
        this.testEntityManager.persistAndFlush(author1);
        this.testEntityManager.persistAndFlush(author1);
        ReportEntity reportEntity1 = new ReportEntity();
        reportEntity1.setAuthor(author1);
        reportEntity1.setTitle("some title");
        reportEntity1.setState(ReportState.OPEN);
        ReportEntity reportEntity2 = new ReportEntity();
        reportEntity2.setAuthor(author1);
        reportEntity2.setTitle("other title");
        reportEntity2.setState(ReportState.OPEN);
        this.testEntityManager.persistAndFlush(reportEntity1);
        this.testEntityManager.persistAndFlush(reportEntity2);

        List<ReportEntity> reports = this.reportRepository.findAllByAuthor_IdAndStateAndTitleContains(
                author1.getId(),
                ReportState.OPEN,
                "other");

        assertThat(reports.size()).isEqualTo(1);
        assertThat(reports.get(0).getAuthor().getId()).isEqualTo(author1.getId());
        assertThat(reports.get(0).getState()).isEqualTo(ReportState.OPEN);
        assertThat(reports.get(0).getTitle()).isEqualTo(reportEntity2.getTitle());
    }

    @Test
    void findAllByTitleContains() {
        ReportEntity reportEntity1 = new ReportEntity();
        reportEntity1.setTitle("some title");
        ReportEntity reportEntity2 = new ReportEntity();
        reportEntity2.setTitle("other title");
        ReportEntity reportEntity3 = new ReportEntity();
        reportEntity3.setTitle("different");
        this.testEntityManager.persistAndFlush(reportEntity1);
        this.testEntityManager.persistAndFlush(reportEntity2);
        this.testEntityManager.persistAndFlush(reportEntity3);

        List<ReportEntity> reports = this.reportRepository.findAllByTitleContains("title");

        assertThat(reports.size()).isEqualTo(2);
    }

    @Test
    void findAllByState() {
        ReportEntity reportEntity1 = new ReportEntity();
        reportEntity1.setState(ReportState.OPEN);
        ReportEntity reportEntity2 = new ReportEntity();
        reportEntity2.setState(ReportState.OPEN);
        ReportEntity reportEntity3 = new ReportEntity();
        reportEntity3.setState(ReportState.APPROVED);
        this.testEntityManager.persistAndFlush(reportEntity1);
        this.testEntityManager.persistAndFlush(reportEntity2);
        this.testEntityManager.persistAndFlush(reportEntity3);

        List<ReportEntity> reports = this.reportRepository.findAllByState(ReportState.OPEN);

        assertThat(reports.size()).isEqualTo(2);
    }

    @Test
    void findAllByStateAndTitleContains() {
        ReportEntity reportEntity1 = new ReportEntity();
        reportEntity1.setTitle("some title");
        reportEntity1.setState(ReportState.OPEN);
        ReportEntity reportEntity2 = new ReportEntity();
        reportEntity2.setTitle("other title");
        reportEntity2.setState(ReportState.OPEN);
        ReportEntity reportEntity3 = new ReportEntity();
        reportEntity3.setTitle("different");
        reportEntity3.setState(ReportState.APPROVED);
        this.testEntityManager.persistAndFlush(reportEntity1);
        this.testEntityManager.persistAndFlush(reportEntity2);
        this.testEntityManager.persistAndFlush(reportEntity3);

        List<ReportEntity> reports = this.reportRepository.findAllByStateAndTitleContains(
                ReportState.OPEN,
                "title");

        assertThat(reports.size()).isEqualTo(2);
    }

    @Test
    void findByIdAndAuthor_Id() {
        UserEntity author1 = new UserEntity("token1", "password", Role.USER);
        this.testEntityManager.persistAndFlush(author1);
        this.testEntityManager.persistAndFlush(author1);
        ReportEntity reportEntity1 = new ReportEntity();
        reportEntity1.setAuthor(author1);
        reportEntity1.setTitle("some title");
        reportEntity1.setState(ReportState.OPEN);
        ReportEntity reportEntity2 = new ReportEntity();
        reportEntity2.setAuthor(author1);
        reportEntity2.setTitle("other title");
        reportEntity2.setState(ReportState.OPEN);
        this.testEntityManager.persistAndFlush(reportEntity1);
        this.testEntityManager.persistAndFlush(reportEntity2);

        ReportEntity reportEntity = this.reportRepository.findByIdAndAuthor_Id(reportEntity1.getId(), author1.getId());

        assertThat(reportEntity.getTitle()).isEqualTo(reportEntity1.getTitle());
    }
}
