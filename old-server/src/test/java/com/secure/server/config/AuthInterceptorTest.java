package com.secure.server.config;

import com.secure.server.entities.Role;
import com.secure.server.services.AuthService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AuthInterceptorTest {

    @MockBean
    private AuthService authService;

    @Autowired
    private AuthInterceptor authInterceptor;

    @Test
    void testPreHandle() {
        String source = "source";
        HttpServletResponse responseMock = mock(HttpServletResponse.class);
        HttpServletRequest requestMock = mock(HttpServletRequest.class);
        HandlerMethod handlerMock = mock(HandlerMethod.class);
        Authorized authorizedMock = mock(Authorized.class);
        when(requestMock.getHeader("auth")).thenReturn(source);
        when(handlerMock.hasMethodAnnotation(Authorized.class)).thenReturn(true);
        when(handlerMock.getMethodAnnotation(Authorized.class)).thenReturn(authorizedMock);
        when(authorizedMock.roles()).thenReturn(new Role[] { Role.USER, Role.EMPLOYEE, Role.ADMIN });
        when(this.authService.isAuthorized(source, Arrays.asList(authorizedMock.roles()))).thenReturn(true);

        this.authInterceptor.preHandle(requestMock, responseMock, handlerMock);

        assertThatNoException();
    }

    @Test
    void testPreHandle_unauthorized() {
        String source = "source";
        HttpServletResponse responseMock = mock(HttpServletResponse.class);
        HttpServletRequest requestMock = mock(HttpServletRequest.class);
        HandlerMethod handlerMock = mock(HandlerMethod.class);
        Authorized authorizedMock = mock(Authorized.class);
        when(requestMock.getHeader("auth")).thenReturn(source);
        when(handlerMock.hasMethodAnnotation(Authorized.class)).thenReturn(true);
        when(handlerMock.getMethodAnnotation(Authorized.class)).thenReturn(authorizedMock);
        when(authorizedMock.roles()).thenReturn(new Role[] { Role.USER, Role.EMPLOYEE, Role.ADMIN });
        when(this.authService.isAuthorized(source, Arrays.asList(authorizedMock.roles()))).thenReturn(false);

        assertThatThrownBy(
                () -> this.authInterceptor.preHandle(requestMock, responseMock, handlerMock),
                "Unauthorized!");
    }
}
