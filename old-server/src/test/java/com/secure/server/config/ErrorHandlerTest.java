package com.secure.server.config;

import com.secure.server.controllers.models.ExceptionModel;
import com.secure.server.exceptions.RequestReadException;
import com.secure.server.exceptions.UnauthorizedException;
import com.secure.server.exceptions.ValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class ErrorHandlerTest {

    @Autowired
    private ErrorHandler errorHandler;

    @Test
    void handleRequestUnauthorizedException() {
        String expectedMessage = "message";
        UnauthorizedException exception = new UnauthorizedException(expectedMessage);
        ResponseEntity<ExceptionModel> expected = new ResponseEntity<>(
                new ExceptionModel(exception.getMessage()), HttpStatus.UNAUTHORIZED);

        ResponseEntity<ExceptionModel> result = errorHandler.handleRequestUnauthorizedException(exception);

        assertThat(result.getStatusCodeValue()).isEqualTo(expected.getStatusCodeValue());
    }

    @Test
    void handleRequestReadException() {
        RequestReadException exception = new RequestReadException();
        ResponseEntity<ExceptionModel> expected = new ResponseEntity<>(
                new ExceptionModel(exception.getMessage()), HttpStatus.BAD_REQUEST);

        ResponseEntity<ExceptionModel> result = errorHandler.handleRequestReadException(exception);

        assertThat(result.getStatusCodeValue()).isEqualTo(expected.getStatusCodeValue());
    }

    @Test
    void handleValidationException() {
        String expectedMessage = "message";
        ValidationException exception = new ValidationException(expectedMessage);
        ResponseEntity<ExceptionModel> expected = new ResponseEntity<>(
                new ExceptionModel(exception.getMessage()), HttpStatus.BAD_REQUEST);

        ResponseEntity<ExceptionModel> result = errorHandler.handleValidationException(exception);

        assertThat(result.getStatusCodeValue()).isEqualTo(expected.getStatusCodeValue());
    }
}